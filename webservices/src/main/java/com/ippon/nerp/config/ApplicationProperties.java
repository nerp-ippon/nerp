package com.ippon.nerp.config;

import com.ippon.nerp.GeneratedByJHipster;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Nerp.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link tech.jhipster.config.JHipsterProperties} for a good example.
 */
@GeneratedByJHipster
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {}
