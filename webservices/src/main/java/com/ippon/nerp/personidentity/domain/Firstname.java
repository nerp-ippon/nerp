package com.ippon.nerp.personidentity.domain;

import com.ippon.nerp.error.domain.Assert;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public record Firstname(String firstname) implements Comparable<Firstname> {
  private static final Pattern FIRSTNAME_PATTERN = Pattern.compile("(\\p{L}+)(\\P{L}*)");

  public Firstname(String firstname) {
    Assert.field("firstname", firstname).notBlank().maxLength(100);

    this.firstname = buildFirstname(firstname);
  }

  private String buildFirstname(String firstname) {
    Matcher matcher = FIRSTNAME_PATTERN.matcher(firstname);
    StringBuilder result = new StringBuilder();

    while (matcher.find()) {
      result.append(StringUtils.capitalize(matcher.group(1).toLowerCase())).append(matcher.group(2));
    }

    return result.toString();
  }

  public String get() {
    return firstname();
  }

  @Override
  public int compareTo(Firstname other) {
    if (other == null) {
      return -1;
    }

    return firstname.compareTo(other.firstname);
  }
}
