package com.ippon.nerp.security;

import com.ippon.nerp.GeneratedByJHipster;
import com.ippon.nerp.authentication.infrastructure.primary.SecurityUtils;
import com.ippon.nerp.config.Constants;
import java.util.Optional;
import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

/**
 * Implementation of {@link AuditorAware} based on Spring Security.
 */
@Component
@GeneratedByJHipster
public class SpringSecurityAuditorAware implements AuditorAware<String> {

  @Override
  public Optional<String> getCurrentAuditor() {
    return Optional.of(SecurityUtils.getOptionalUserLogin().orElse(Constants.SYSTEM));
  }
}
