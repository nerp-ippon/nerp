package com.ippon.nerp.kipe;

import static com.ippon.nerp.kipe.Action.*;
import static com.ippon.nerp.kipe.Resource.*;

import com.ippon.nerp.authentication.domain.Authorities;
import com.ippon.nerp.authentication.domain.Username;
import com.ippon.nerp.authentication.infrastructure.primary.NotAuthenticatedUserException;
import com.ippon.nerp.authentication.infrastructure.primary.SecurityUtils;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public final class NerpAuthorizations {

  private static final Actions DEFAULT_ACTIONS = new Actions();

  private static final Actions NO_ROLE_ACTIONS = new Actions(specific("read", USERS), all("read", INVOICES));

  private static final Map<String, Actions> ROLES_ACTIONS = new RoleActionsBuilder()
    .append(Authorities.ADMIN, adminActions())
    .append(Authorities.MARKET, marketActions())
    .build();

  private NerpAuthorizations() {}

  private static Actions adminActions() {
    return Actions.builder().add(all("read", USERS)).add(all("save", USERS)).add(all("create", INVOICES)).build();
  }

  private static Actions marketActions() {
    return Actions.builder().add(all("create", INVOICES)).build();
  }

  public static Username getUsername(Authentication authentication) {
    return SecurityUtils.extractPrincipal(authentication).map(Username::new).orElseThrow(NotAuthenticatedUserException::new);
  }

  public static boolean allAuthorized(Authentication authentication, String action, Resource resource) {
    if (missingAuthorizationInformation(authentication, action, resource)) {
      return false;
    }

    if (NO_ROLE_ACTIONS.allAuthorized(action, resource)) {
      return true;
    }

    return canMakeAction(authentication, canDoAll(action, resource));
  }

  private static Predicate<String> canDoAll(String action, Resource resource) {
    return authority -> userActions(authority).allAuthorized(action, resource);
  }

  public static boolean specificAuthorized(Authentication authentication, String action, Resource resource) {
    if (missingAuthorizationInformation(authentication, action, resource)) {
      return false;
    }

    if (NO_ROLE_ACTIONS.specificAuthorized(action, resource)) {
      return true;
    }

    return canMakeAction(authentication, canDoSpecific(action, resource));
  }

  private static boolean missingAuthorizationInformation(Authentication authentication, String action, Resource resource) {
    return authentication == null || StringUtils.isBlank(action) || resource == null;
  }

  private static boolean canMakeAction(Authentication authentication, Predicate<String> actionChecker) {
    return streamAuthorities(authentication).anyMatch(actionChecker);
  }

  private static Stream<String> streamAuthorities(Authentication authentication) {
    return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority);
  }

  private static Predicate<String> canDoSpecific(String action, Resource resource) {
    return authority -> userActions(authority).specificAuthorized(action, resource);
  }

  private static Actions userActions(String authority) {
    return ROLES_ACTIONS.getOrDefault(authority, DEFAULT_ACTIONS);
  }

  private static class RoleActionsBuilder {

    private final Map<String, Actions> actions = new HashMap<>();

    private RoleActionsBuilder append(String role, Actions actions) {
      this.actions.put(role, actions);

      return this;
    }

    private Map<String, Actions> build() {
      return actions;
    }
  }
}
