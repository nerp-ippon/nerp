/**
 *
 */
package com.ippon.nerp.error.infrastructure.primary;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.ippon.nerp.error.infrastructure.primary.NerpFieldError.NerpFieldErrorBuilder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonDeserialize(builder = NerpFieldErrorBuilder.class)
@ApiModel(description = "Error for a field validation")
class NerpFieldError {

  @ApiModelProperty(value = "Path to the field in error", example = "address.country", required = true)
  private final String fieldPath;

  @ApiModelProperty(value = "Technical reason for the invalidation", example = "user.mandatory", required = true)
  private final String reason;

  @ApiModelProperty(value = "Human readable message for the invalidation", example = "Le champ doit être renseigné", required = true)
  private final String message;

  private NerpFieldError(NerpFieldErrorBuilder builder) {
    fieldPath = builder.fieldPath;
    reason = builder.reason;
    message = builder.message;
  }

  public static NerpFieldErrorBuilder builder() {
    return new NerpFieldErrorBuilder();
  }

  public String getFieldPath() {
    return fieldPath;
  }

  public String getReason() {
    return reason;
  }

  public String getMessage() {
    return message;
  }

  @JsonPOJOBuilder(withPrefix = "")
  public static class NerpFieldErrorBuilder {

    private String fieldPath;
    private String reason;
    private String message;

    public NerpFieldErrorBuilder fieldPath(String fieldPath) {
      this.fieldPath = fieldPath;

      return this;
    }

    public NerpFieldErrorBuilder reason(String reason) {
      this.reason = reason;

      return this;
    }

    public NerpFieldErrorBuilder message(String message) {
      this.message = message;

      return this;
    }

    public NerpFieldError build() {
      return new NerpFieldError(this);
    }
  }
}
