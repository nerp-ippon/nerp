package com.ippon.nerp.error.infrastructure.primary;

import com.ippon.nerp.error.domain.ErrorKey;

enum AuthenticationErrorKey implements ErrorKey {
  NOT_AUTHENTICATED("user.not-authenticated");

  private final String key;

  private AuthenticationErrorKey(String key) {
    this.key = key;
  }

  @Override
  public String key() {
    return key;
  }
}
