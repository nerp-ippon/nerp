package com.ippon.nerp.error.infrastructure.primary;

import com.ippon.nerp.error.domain.Assert;
import com.ippon.nerp.error.domain.ErrorStatus;
import org.springframework.http.HttpStatus;

enum HttpErrorStatus {
  INTERNAL_SERVER_ERROR(500),
  NOT_FOUND(404),
  BAD_REQUEST(400),
  FORBIDDEN(403),
  UNAUTHORIZED(401);

  private final HttpStatus httpStatus;

  private HttpErrorStatus(int code) {
    httpStatus = HttpStatus.valueOf(code);
  }

  public HttpStatus httpStatus() {
    return httpStatus;
  }

  static HttpErrorStatus from(ErrorStatus status) {
    Assert.notNull("status", status);

    return HttpErrorStatus.valueOf(status.name());
  }
}
