package com.ippon.nerp.error.domain;

public class MissingMandatoryValueException extends NerpException {

  public MissingMandatoryValueException(String field) {
    super(
      NerpException.builder(AssertErrorKey.MISSING_MANDATORY_VALUE).message("Missing mandatory value in " + field).argument("field", field)
    );
  }
}
