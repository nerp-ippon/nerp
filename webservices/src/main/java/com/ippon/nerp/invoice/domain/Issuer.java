package com.ippon.nerp.invoice.domain;

public record Issuer(OrganisationName name) {
  public Issuer(String name) {
    this(new OrganisationName(name));
  }
}
