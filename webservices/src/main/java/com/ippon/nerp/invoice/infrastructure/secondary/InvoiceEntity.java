package com.ippon.nerp.invoice.infrastructure.secondary;

import com.ippon.nerp.invoice.domain.Address;
import com.ippon.nerp.invoice.domain.AddressLine;
import com.ippon.nerp.invoice.domain.Invoice;
import com.ippon.nerp.invoice.domain.Invoice.InvoiceBuilder;
import com.ippon.nerp.invoice.domain.InvoiceId;
import com.ippon.nerp.invoice.domain.Issuer;
import com.ippon.nerp.invoice.domain.Recipient;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.BiConsumer;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "invoice")
class InvoiceEntity {

  private static final transient Map<Integer, BiConsumer<InvoiceEntity, String>> ADDRESSES_LINES_VALUATOR = buildAddressesLinesValuators();

  @Id
  @Column(name = "id")
  private UUID id;

  @Column(name = "recipient_name")
  private String recipientName;

  @Column(name = "recipient_address_line_1")
  private String recipientAddressLine1;

  @Column(name = "recipient_address_line_2")
  private String recipientAddressLine2;

  @Column(name = "recipient_address_line_3")
  private String recipientAddressLine3;

  @Column(name = "recipient_address_line_4")
  private String recipientAddressLine4;

  @Column(name = "recipient_address_line_5")
  private String recipientAddressLine5;

  @Column(name = "recipient_address_line_6")
  private String recipientAddressLine6;

  @Column(name = "recipient_address_line_7")
  private String recipientAddressLine7;

  @Column(name = "recipient_vat_number")
  private String recipientVatNumber;

  @Column(name = "recipient_contract")
  private String recipientContract;

  @Column(name = "issuer_name")
  private String issuerName;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinColumn(referencedColumnName = "id", name = "invoice")
  private Collection<LineEntity> lines;

  private static Map<Integer, BiConsumer<InvoiceEntity, String>> buildAddressesLinesValuators() {
    Map<Integer, BiConsumer<InvoiceEntity, String>> valuators = new HashMap<>();

    valuators.put(0, InvoiceEntity::recipientAddressLine1);
    valuators.put(1, InvoiceEntity::recipientAddressLine2);
    valuators.put(2, InvoiceEntity::recipientAddressLine3);
    valuators.put(3, InvoiceEntity::recipientAddressLine4);
    valuators.put(4, InvoiceEntity::recipientAddressLine5);
    valuators.put(5, InvoiceEntity::recipientAddressLine6);
    valuators.put(6, InvoiceEntity::recipientAddressLine7);

    return valuators;
  }

  public static InvoiceEntity from(Invoice invoice) {
    if (invoice == null) {
      return null;
    }

    InvoiceEntity entity = new InvoiceEntity().id(invoice.id().get()).issuerName(invoice.issuer().name().get()).lines(buildLines(invoice));

    Recipient recipient = invoice.recipient();
    entity
      .recipientName(recipient.name().get())
      .recipientVatNumber(recipient.customerVatNumber().get())
      .recipientContract(recipient.reference().get());

    valuateAddress(recipient, entity);

    return entity;
  }

  private static void valuateAddress(Recipient recipient, InvoiceEntity entity) {
    List<AddressLine> addressLines = recipient.address().lines();

    IntStream
      .range(0, addressLines.size())
      .forEach(index -> ADDRESSES_LINES_VALUATOR.get(index).accept(entity, addressLines.get(index).get()));
  }

  private static List<LineEntity> buildLines(Invoice invoice) {
    return IntStream
      .range(0, invoice.lines().size())
      .mapToObj(index -> LineEntity.from(invoice.id().get(), index, invoice.lines().get(index)))
      .toList();
  }

  public Invoice toDomain() {
    InvoiceBuilder builder = Invoice.builder().id(new InvoiceId(id)).recipient(buildRecipient()).issuer(new Issuer(issuerName));

    lines.stream().map(LineEntity::toDomain).forEach(builder::line);

    return builder.build();
  }

  private Recipient buildRecipient() {
    return Recipient
      .builder()
      .name(recipientName)
      .address(buildAddress())
      .customerVatNumber(recipientVatNumber)
      .contract(recipientContract)
      .build();
  }

  private Address buildAddress() {
    List<AddressLine> lines = Stream
      .of(
        recipientAddressLine1,
        recipientAddressLine2,
        recipientAddressLine3,
        recipientAddressLine4,
        recipientAddressLine5,
        recipientAddressLine6,
        recipientAddressLine7
      )
      .filter(StringUtils::isNotBlank)
      .map(AddressLine::new)
      .toList();

    return new Address(lines);
  }

  public InvoiceEntity id(UUID id) {
    this.id = id;

    return this;
  }

  public InvoiceEntity recipientName(String recipientName) {
    this.recipientName = recipientName;

    return this;
  }

  public InvoiceEntity recipientAddressLine1(String recipientAddressLine1) {
    this.recipientAddressLine1 = recipientAddressLine1;

    return this;
  }

  public InvoiceEntity recipientAddressLine2(String recipientAddressLine2) {
    this.recipientAddressLine2 = recipientAddressLine2;

    return this;
  }

  public InvoiceEntity recipientAddressLine3(String recipientAddressLine3) {
    this.recipientAddressLine3 = recipientAddressLine3;

    return this;
  }

  public InvoiceEntity recipientAddressLine4(String recipientAddressLine4) {
    this.recipientAddressLine4 = recipientAddressLine4;

    return this;
  }

  public InvoiceEntity recipientAddressLine5(String recipientAddressLine5) {
    this.recipientAddressLine5 = recipientAddressLine5;

    return this;
  }

  public InvoiceEntity recipientAddressLine6(String recipientAddressLine6) {
    this.recipientAddressLine6 = recipientAddressLine6;

    return this;
  }

  public InvoiceEntity recipientAddressLine7(String recipientAddressLine7) {
    this.recipientAddressLine7 = recipientAddressLine7;

    return this;
  }

  public InvoiceEntity recipientVatNumber(String recipientVatNumber) {
    this.recipientVatNumber = recipientVatNumber;

    return this;
  }

  public InvoiceEntity recipientContract(String recipientContract) {
    this.recipientContract = recipientContract;

    return this;
  }

  public InvoiceEntity issuerName(String issuerName) {
    this.issuerName = issuerName;

    return this;
  }

  public InvoiceEntity lines(Collection<LineEntity> lines) {
    this.lines = lines;

    return this;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(id).hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    InvoiceEntity other = (InvoiceEntity) obj;
    return new EqualsBuilder().append(id, other.id).isEquals();
  }
}
