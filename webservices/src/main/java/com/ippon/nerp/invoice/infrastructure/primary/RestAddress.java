package com.ippon.nerp.invoice.infrastructure.primary;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.ippon.nerp.common.infrastructure.primary.ValidationMessage;
import com.ippon.nerp.invoice.domain.Address;
import com.ippon.nerp.invoice.domain.AddressLine;
import com.ippon.nerp.invoice.infrastructure.primary.RestAddress.RestAddressBuilder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;
import javax.validation.constraints.NotBlank;

@JsonDeserialize(builder = RestAddressBuilder.class)
@ApiModel(value = "Address", description = "Information for an address")
class RestAddress {

  @ApiModelProperty(value = "First line of this address", required = true, example = "Chez gnuk")
  private final String firstLine;

  @ApiModelProperty(value = "City of this address", required = true, example = "Lyon")
  private final String city;

  @ApiModelProperty(value = "Full country of this address", required = true, example = "FRANCE")
  private final String country;

  private RestAddress(RestAddressBuilder builder) {
    firstLine = builder.firstLine;
    city = builder.city;
    country = builder.country;
  }

  public Address toDomain() {
    return new Address(List.of(new AddressLine(firstLine), new AddressLine(city), new AddressLine(country)));
  }

  @NotBlank(message = ValidationMessage.MISSING_MANDATORY_VALUE)
  public String getFirstLine() {
    return firstLine;
  }

  @NotBlank(message = ValidationMessage.MISSING_MANDATORY_VALUE)
  public String getCity() {
    return city;
  }

  @NotBlank(message = ValidationMessage.MISSING_MANDATORY_VALUE)
  public String getCountry() {
    return country;
  }

  @JsonPOJOBuilder(withPrefix = "")
  static class RestAddressBuilder {

    private String firstLine;
    private String city;
    private String country;

    public RestAddressBuilder firstLine(String firstLine) {
      this.firstLine = firstLine;

      return this;
    }

    public RestAddressBuilder city(String city) {
      this.city = city;

      return this;
    }

    public RestAddressBuilder country(String country) {
      this.country = country;

      return this;
    }

    RestAddress build() {
      return new RestAddress(this);
    }
  }
}
