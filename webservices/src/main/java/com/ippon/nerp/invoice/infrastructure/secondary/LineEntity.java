package com.ippon.nerp.invoice.infrastructure.secondary;

import com.ippon.nerp.invoice.domain.Currency;
import com.ippon.nerp.invoice.domain.Line;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@Table(name = "invoice_line")
@IdClass(LineEntityId.class)
class LineEntity {

  @Id
  @Column(name = "invoice")
  private UUID invoice;

  @Id
  @Column(name = "number")
  private int number;

  @Column(name = "designation")
  private String designation;

  @Column(name = "days")
  private double days;

  @Column(name = "amount")
  private double amount;

  @Column(name = "currency")
  @Enumerated(EnumType.STRING)
  private Currency currency;

  public static LineEntity from(UUID invoice, int number, Line line) {
    if (invoice == null || line == null) {
      return null;
    }

    return new LineEntity()
      .invoice(invoice)
      .number(number)
      .designation(line.designation().get())
      .days(line.days().get().doubleValue())
      .amount(line.fee().amount().doubleValue())
      .currency(line.fee().currency());
  }

  public Line toDomain() {
    return Line.builder().designation(designation).days(days).amount(amount).currency(currency).build();
  }

  private LineEntity invoice(UUID invoice) {
    this.invoice = invoice;

    return this;
  }

  private LineEntity number(int number) {
    this.number = number;

    return this;
  }

  private LineEntity designation(String designation) {
    this.designation = designation;

    return this;
  }

  private LineEntity days(double days) {
    this.days = days;

    return this;
  }

  private LineEntity amount(double amount) {
    this.amount = amount;

    return this;
  }

  private LineEntity currency(Currency currency) {
    this.currency = currency;

    return this;
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(invoice).append(number).hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    LineEntity other = (LineEntity) obj;
    return new EqualsBuilder().append(invoice, other.invoice).append(number, other.number).isEquals();
  }
}
