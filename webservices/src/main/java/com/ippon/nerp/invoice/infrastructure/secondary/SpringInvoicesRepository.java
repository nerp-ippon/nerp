package com.ippon.nerp.invoice.infrastructure.secondary;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface SpringInvoicesRepository extends JpaRepository<InvoiceEntity, UUID> {}
