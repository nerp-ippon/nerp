package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.Assert;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Invoice {

  private final InvoiceId id;
  private final Recipient recipient;
  private final Issuer issuer;
  private final List<Line> lines;
  private final Total total;

  private Invoice(InvoiceBuilder builder) {
    Assert.notNull("id", builder.id);
    Assert.notNull("recipient", builder.recipient);
    Assert.notNull("issuer", builder.issuer);

    assertHasLine(builder);

    id = builder.id;
    recipient = builder.recipient;
    issuer = builder.issuer;
    lines = Collections.unmodifiableList(builder.lines);
    total = new Total(lines);
  }

  private void assertHasLine(InvoiceBuilder builder) {
    if (builder.lines.isEmpty()) {
      throw InvalidInvoiceException.missingLine();
    }
  }

  public static InvoiceBuilder builder() {
    return new InvoiceBuilder();
  }

  public InvoiceId id() {
    return id;
  }

  public Recipient recipient() {
    return recipient;
  }

  public Issuer issuer() {
    return issuer;
  }

  public List<Line> lines() {
    return lines;
  }

  public Total total() {
    return total;
  }

  public static class InvoiceBuilder {

    private final List<Line> lines = new ArrayList<>();

    private InvoiceId id;
    private Recipient recipient;
    private Issuer issuer;

    public InvoiceBuilder id(InvoiceId id) {
      this.id = id;

      return this;
    }

    public InvoiceBuilder recipient(Recipient recipient) {
      this.recipient = recipient;

      return this;
    }

    public InvoiceBuilder issuer(Issuer issuer) {
      this.issuer = issuer;

      return this;
    }

    public InvoiceBuilder line(Line line) {
      lines.add(line);

      return this;
    }

    public Invoice build() {
      return new Invoice(this);
    }
  }
}
