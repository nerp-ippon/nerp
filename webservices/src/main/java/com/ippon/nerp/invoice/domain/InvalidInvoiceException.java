package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.NerpException;

class InvalidInvoiceException extends NerpException {

  private InvalidInvoiceException(NERPExceptionBuilder builder) {
    super(builder);
  }

  public static InvalidInvoiceException missingLine() {
    return new InvalidInvoiceException(NerpException.builder(InvoiceErrorKey.MISSING_LINES).message("Can't build invoice without line"));
  }
}
