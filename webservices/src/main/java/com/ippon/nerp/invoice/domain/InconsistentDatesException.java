package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.NerpException;

class InconsistentDatesException extends NerpException {

  public InconsistentDatesException() {
    super(NerpException.builder(InvoiceErrorKey.INCONSISTENT_DATES).message("Invoice dates are inconsistent (wrong order)"));
  }
}
