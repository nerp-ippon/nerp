package com.ippon.nerp.invoice.infrastructure.primary;

import com.ippon.nerp.invoice.domain.Invoice;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;

@ApiModel(value = "invoice", description = "Information for an invoice")
class RestInvoice {

  @ApiModelProperty(value = "ID of the invoice", required = true)
  private final UUID id;

  @ApiModelProperty(value = "Name of the recipient for this invoice", required = true)
  private final String recipient;

  @ApiModelProperty(value = "Total amount of this invoice", required = true)
  private final double total;

  public RestInvoice(UUID id, String recipient, double total) {
    this.id = id;
    this.recipient = recipient;
    this.total = total;
  }

  public static RestInvoice from(Invoice invoice) {
    if (invoice == null) {
      return null;
    }

    return new RestInvoice(invoice.id().get(), invoice.recipient().name().get(), invoice.total().includingTaxes().doubleValue());
  }

  public UUID getId() {
    return id;
  }

  public String getRecipient() {
    return recipient;
  }

  public double getTotal() {
    return total;
  }
}
