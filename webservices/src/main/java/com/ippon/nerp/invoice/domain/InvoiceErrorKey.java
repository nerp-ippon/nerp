package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.ErrorKey;

enum InvoiceErrorKey implements ErrorKey {
  INVALID_ADDRESS("invoice.invalid-address"),
  INCONSISTENT_DATES("invoice.inconstitent-dates"),
  INVALID_DAYS("invoice.invalid-days"),
  MISSING_LINES("invoice.missing-lines"),
  INVALID_VAT_NUMBER("invoice.invalid-vat-number");

  private final String key;

  private InvoiceErrorKey(String key) {
    this.key = key;
  }

  @Override
  public String key() {
    return key;
  }
}
