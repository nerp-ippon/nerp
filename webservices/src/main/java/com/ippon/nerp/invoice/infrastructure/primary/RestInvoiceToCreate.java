package com.ippon.nerp.invoice.infrastructure.primary;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.ippon.nerp.common.infrastructure.primary.ValidationMessage;
import com.ippon.nerp.invoice.domain.InvoiceToCreate;
import com.ippon.nerp.invoice.domain.InvoiceToCreate.InvoiceToCreateBuilder;
import com.ippon.nerp.invoice.domain.Issuer;
import com.ippon.nerp.invoice.infrastructure.primary.RestInvoiceToCreate.RestInvoiceToCreateBuilder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Collection;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@JsonDeserialize(builder = RestInvoiceToCreateBuilder.class)
@ApiModel(value = "InvoiceToCreate", description = "Information for an invoice to create")
class RestInvoiceToCreate {

  @ApiModelProperty(value = "Name of this invoice's issuer", required = true, example = "Colin DAMON")
  private final String issuer;

  @ApiModelProperty(value = "Recipient for this invoice", required = true)
  private final RestRecipient recipient;

  @ApiModelProperty(value = "Lines of this invoice", required = true)
  private final Collection<RestLine> lines;

  private RestInvoiceToCreate(RestInvoiceToCreateBuilder builder) {
    issuer = builder.issuer;
    recipient = builder.recipient;
    lines = builder.lines;
  }

  public InvoiceToCreate toDomain() {
    InvoiceToCreateBuilder builder = InvoiceToCreate.builder().issuer(new Issuer(issuer)).recipient(recipient.toDomain());

    lines.stream().map(RestLine::toDomain).forEach(builder::line);

    return builder.build();
  }

  @NotBlank(message = ValidationMessage.MISSING_MANDATORY_VALUE)
  public String getIssuer() {
    return issuer;
  }

  @Valid
  @NotNull(message = ValidationMessage.MISSING_MANDATORY_VALUE)
  public RestRecipient getRecipient() {
    return recipient;
  }

  @NotEmpty(message = ValidationMessage.MISSING_MANDATORY_VALUE)
  public Collection<RestLine> getLines() {
    return lines;
  }

  @JsonPOJOBuilder(withPrefix = "")
  static class RestInvoiceToCreateBuilder {

    private String issuer;
    private RestRecipient recipient;
    private Collection<RestLine> lines;

    RestInvoiceToCreateBuilder issuer(String issuer) {
      this.issuer = issuer;

      return this;
    }

    RestInvoiceToCreateBuilder recipient(RestRecipient recipient) {
      this.recipient = recipient;

      return this;
    }

    RestInvoiceToCreateBuilder lines(Collection<RestLine> lines) {
      this.lines = lines;

      return this;
    }

    RestInvoiceToCreate build() {
      return new RestInvoiceToCreate(this);
    }
  }
}
