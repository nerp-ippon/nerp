package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.Assert;

public record OrganisationName(String name) {
  public OrganisationName(String name) {
    Assert.notBlank("name", name);

    this.name = name;
  }

  public String get() {
    return name();
  }
}
