package com.ippon.nerp.invoice.application;

import com.ippon.nerp.invoice.domain.InvoiceToCreate;
import com.ippon.nerp.kipe.AccessChecker;
import com.ippon.nerp.kipe.NerpAuthorizations;
import com.ippon.nerp.kipe.Resource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
class InvoiceToCreateAccessChecker implements AccessChecker<InvoiceToCreate> {

  @Override
  public boolean can(Authentication authentication, String action, InvoiceToCreate item) {
    return NerpAuthorizations.allAuthorized(authentication, action, Resource.INVOICES);
  }
}
