package com.ippon.nerp.invoice.domain;

import java.math.BigDecimal;

public record Line(Designation designation, Days days, Fee fee) {
  private Line(LineBuilder builder) {
    this(new Designation(builder.designation), new Days(builder.days), new Fee(builder.amount, builder.currency));
  }

  public static LineBuilder builder() {
    return new LineBuilder();
  }

  public BigDecimal total() {
    return fee.amount().multiply(days.get());
  }

  public static class LineBuilder {

    private String designation;
    private double days;
    private double amount;
    private Currency currency;

    public LineBuilder designation(String designation) {
      this.designation = designation;

      return this;
    }

    public LineBuilder days(double days) {
      this.days = days;

      return this;
    }

    public LineBuilder amount(double amount) {
      this.amount = amount;

      return this;
    }

    public LineBuilder currency(Currency currency) {
      this.currency = currency;

      return this;
    }

    public Line build() {
      return new Line(this);
    }
  }
}
