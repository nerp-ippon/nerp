package com.ippon.nerp.invoice.application;

import com.ippon.nerp.invoice.domain.InvoiceId;
import com.ippon.nerp.kipe.AccessChecker;
import com.ippon.nerp.kipe.NerpAuthorizations;
import com.ippon.nerp.kipe.Resource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
class InvoiceIdAccessChecker implements AccessChecker<InvoiceId> {

  @Override
  public boolean can(Authentication authentication, String action, InvoiceId item) {
    return NerpAuthorizations.allAuthorized(authentication, action, Resource.INVOICES);
  }
}
