package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.Assert;

public record AddressLine(String line) {
  public AddressLine(String line) {
    Assert.field("line", line).notBlank().maxLength(38);

    this.line = line.replaceAll("\\s", " ").replaceAll(" +", " ").trim();
  }

  public String get() {
    return line();
  }
}
