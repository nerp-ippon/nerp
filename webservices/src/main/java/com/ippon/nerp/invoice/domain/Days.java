package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.Assert;
import java.math.BigDecimal;

public record Days(BigDecimal days) {
  public Days(double days) {
    this(getDays(days));
  }

  private static BigDecimal getDays(double days) {
    Assert.field("days", days).min(0.25);

    if (days % 0.25 != 0) {
      throw new InvalidDaysException(days);
    }

    return BigDecimal.valueOf(days);
  }

  public BigDecimal get() {
    return days();
  }
}
