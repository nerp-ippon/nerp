package com.ippon.nerp.invoice.infrastructure.primary;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.ippon.nerp.common.infrastructure.primary.ValidationMessage;
import com.ippon.nerp.invoice.domain.Currency;
import com.ippon.nerp.invoice.domain.Line;
import com.ippon.nerp.invoice.infrastructure.primary.RestLine.RestLineBuilder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotBlank;

@JsonDeserialize(builder = RestLineBuilder.class)
@ApiModel(value = "Line", description = "Line of an invoice")
class RestLine {

  @ApiModelProperty(value = "Desgination of this line", required = true, example = "Salaire")
  private final String designation;

  @ApiModelProperty(value = "Number of days for this line", required = true)
  private final double days;

  @ApiModelProperty(value = "Amount for a day", required = true)
  private final double amount;

  private RestLine(RestLineBuilder builder) {
    designation = builder.designation;
    days = builder.days;
    amount = builder.amount;
  }

  public Line toDomain() {
    return Line.builder().designation(designation).days(days).amount(amount).currency(Currency.EUR).build();
  }

  @NotBlank(message = ValidationMessage.MISSING_MANDATORY_VALUE)
  public String getDesignation() {
    return designation;
  }

  public double getDays() {
    return days;
  }

  public double getAmount() {
    return amount;
  }

  @JsonPOJOBuilder(withPrefix = "")
  static class RestLineBuilder {

    private String designation;
    private double days;
    private double amount;

    RestLineBuilder designation(String designation) {
      this.designation = designation;

      return this;
    }

    RestLineBuilder days(double days) {
      this.days = days;

      return this;
    }

    RestLineBuilder amount(double amount) {
      this.amount = amount;

      return this;
    }

    RestLine build() {
      return new RestLine(this);
    }
  }
}
