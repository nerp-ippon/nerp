package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.NerpException;

class InvalidVatNumberException extends NerpException {

  public InvalidVatNumberException(String vatNumber) {
    super(
      NerpException
        .builder(InvoiceErrorKey.INVALID_VAT_NUMBER)
        .message("An invalid VAT number was found: " + vatNumber)
        .argument("number", vatNumber)
    );
  }
}
