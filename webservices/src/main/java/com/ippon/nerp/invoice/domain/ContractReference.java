package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.Assert;

public record ContractReference(String reference) {
  public ContractReference(String reference) {
    Assert.notBlank("reference", reference);

    this.reference = reference;
  }

  public String get() {
    return reference();
  }
}
