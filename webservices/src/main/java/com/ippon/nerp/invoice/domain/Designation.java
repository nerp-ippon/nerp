package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.Assert;

public record Designation(String designation) {
  public Designation(String designation) {
    Assert.field("designation", designation).notBlank().maxLength(300);

    this.designation = designation;
  }

  public String get() {
    return designation();
  }
}
