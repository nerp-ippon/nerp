package com.ippon.nerp.invoice.application;

import com.ippon.nerp.invoice.domain.Invoice;
import com.ippon.nerp.invoice.domain.InvoiceId;
import com.ippon.nerp.invoice.domain.InvoiceToCreate;
import com.ippon.nerp.invoice.domain.InvoicesRepository;
import java.util.Optional;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InvoicesApplicationService {

  private final InvoicesRepository invoices;

  public InvoicesApplicationService(InvoicesRepository invoices) {
    this.invoices = invoices;
  }

  @Transactional
  @PreAuthorize("can('create', #toCreate)")
  public Invoice create(InvoiceToCreate toCreate) {
    Invoice invoice = toCreate.newInvoice();

    invoices.save(invoice);

    return invoice;
  }

  @Transactional(readOnly = true)
  @PreAuthorize("can('read', #invoice)")
  public Optional<Invoice> get(InvoiceId invoice) {
    return invoices.get(invoice);
  }
}
