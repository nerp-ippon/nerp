package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.error.domain.NerpException;

class InvalidAddressException extends NerpException {

  public InvalidAddressException() {
    super(NerpException.builder(InvoiceErrorKey.INVALID_ADDRESS).message("Addresses must have between 3 and 7 lines."));
  }
}
