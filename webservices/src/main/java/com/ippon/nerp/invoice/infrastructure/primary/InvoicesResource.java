package com.ippon.nerp.invoice.infrastructure.primary;

import com.ippon.nerp.invoice.application.InvoicesApplicationService;
import com.ippon.nerp.invoice.domain.Invoice;
import com.ippon.nerp.invoice.domain.InvoiceId;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import java.net.URI;
import java.util.UUID;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/invoices")
class InvoicesResource {

  private final InvoicesApplicationService invoices;

  public InvoicesResource(InvoicesApplicationService invoices) {
    this.invoices = invoices;
  }

  @PostMapping
  @ApiOperation("Create an invoice")
  ResponseEntity<RestInvoice> createInvoice(@Validated @RequestBody RestInvoiceToCreate invoice) {
    Invoice result = invoices.create(invoice.toDomain());

    URI uri = URI.create("/api/invoices/" + result.id().get().toString());
    return ResponseEntity.created(uri).body(RestInvoice.from(result));
  }

  @GetMapping("/{invoice-id}")
  @ApiOperation("Get an invoice")
  ResponseEntity<RestInvoice> getInvoice(@PathVariable("invoice-id") @ApiParam("ID of the invoice to get") UUID id) {
    return ResponseEntity.of(invoices.get(new InvoiceId(id)).map(RestInvoice::from));
  }
}
