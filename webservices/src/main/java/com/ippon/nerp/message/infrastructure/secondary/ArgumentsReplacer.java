package com.ippon.nerp.message.infrastructure.secondary;

import java.util.Map;
import java.util.function.BinaryOperator;

final class ArgumentsReplacer {

  private static final String OPEN_MUSTACHE = "\\{\\{\\s*";
  private static final String CLOSE_MUSTACHE = "\\s*\\}\\}";

  private ArgumentsReplacer() {}

  public static String replaceArguments(String message, Map<String, ? extends Object> arguments) {
    if (message == null || arguments == null) {
      return message;
    }

    return arguments.entrySet().stream().reduce(message, ArgumentsReplacer::replaceEntry, nothing());
  }

  private static String replaceEntry(String result, Map.Entry<String, ? extends Object> entry) {
    return result.replaceAll(OPEN_MUSTACHE + entry.getKey() + CLOSE_MUSTACHE, getArgumentValue(entry));
  }

  private static String getArgumentValue(Map.Entry<String, ? extends Object> argument) {
    Object value = argument.getValue();
    if (value == null) {
      return "null";
    }

    return value.toString();
  }

  private static BinaryOperator<String> nothing() {
    return (k, v) -> v;
  }
}
