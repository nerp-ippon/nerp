package com.ippon.nerp.account.infrastructure.secondary;

import com.ippon.nerp.account.domain.Account;
import com.ippon.nerp.account.domain.AccountsRepository;
import org.springframework.stereotype.Repository;

@Repository
class NerpAccountsRepository implements AccountsRepository {

  private final TokensReader tokens;

  public NerpAccountsRepository(TokensReader tokens) {
    this.tokens = tokens;
  }

  @Override
  public Account authenticatedUser() {
    return tokens.read();
  }
}
