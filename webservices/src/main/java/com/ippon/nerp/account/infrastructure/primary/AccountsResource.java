package com.ippon.nerp.account.infrastructure.primary;

import com.ippon.nerp.account.application.AccountsApplicationService;
import com.ippon.nerp.account.domain.Account;
import com.ippon.nerp.common.infrastructure.primary.NerpApis;
import com.ippon.nerp.error.infrastructure.primary.NerpError;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@Api(tags = NerpApis.ACCOUNTS_TAG, description = NerpApis.ACCOUNTS_DESCRIPTION)
class AccountsResource {

  private final AccountsApplicationService accounts;

  public AccountsResource(AccountsApplicationService accounts) {
    this.accounts = accounts;
  }

  @GetMapping("/current-account")
  @ApiOperation("Get information for the authenticated account")
  @ApiResponses(
    { @ApiResponse(code = 500, response = NerpError.class, message = "An error occured while reading the account information") }
  )
  public ResponseEntity<RestUser> getConnectedAccount() {
    Account authenticatedUser = accounts.notSecured().authenticatedUser();
    return ResponseEntity.ok(RestUser.from(authenticatedUser));
  }
}
