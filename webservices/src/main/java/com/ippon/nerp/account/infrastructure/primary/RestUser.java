package com.ippon.nerp.account.infrastructure.primary;

import com.ippon.nerp.account.domain.Account;
import com.ippon.nerp.account.domain.Authority;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Collection;

@ApiModel(value = "User", description = "Information for a user")
class RestUser {

  @ApiModelProperty(value = "Username (id) of this user", required = true)
  private final String username;

  @ApiModelProperty(value = "Email of the user", required = true)
  private final String email;

  @ApiModelProperty(value = "Firstname of the user", required = true)
  private final String firstname;

  @ApiModelProperty(value = "Lastname of the user", required = true)
  private final String lastname;

  @ApiModelProperty("Authorities of the user")
  private final Collection<String> authorities;

  private RestUser(RestUtilisateurBuilder builder) {
    username = builder.username;
    email = builder.email;
    firstname = builder.firstname;
    lastname = builder.lastname;
    authorities = builder.authorities;
  }

  static RestUser from(Account account) {
    if (account == null) {
      return null;
    }

    return builder()
      .username(account.username().get())
      .email(account.email().get())
      .firstname(account.firstname().get())
      .lastname(account.lastname().get())
      .authorities(account.authorities().stream().map(Authority::get).toList())
      .build();
  }

  static RestUtilisateurBuilder builder() {
    return new RestUtilisateurBuilder();
  }

  public String getUsername() {
    return username;
  }

  public String getEmail() {
    return email;
  }

  public String getFirstname() {
    return firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public Collection<String> getAuthorities() {
    return authorities;
  }

  static class RestUtilisateurBuilder {

    private String username;
    private String email;
    private String firstname;
    private String lastname;
    private Collection<String> authorities;

    public RestUtilisateurBuilder username(String username) {
      this.username = username;

      return this;
    }

    public RestUtilisateurBuilder email(String email) {
      this.email = email;

      return this;
    }

    public RestUtilisateurBuilder firstname(String firstname) {
      this.firstname = firstname;

      return this;
    }

    public RestUtilisateurBuilder lastname(String lastname) {
      this.lastname = lastname;

      return this;
    }

    public RestUtilisateurBuilder authorities(Collection<String> authorities) {
      this.authorities = authorities;

      return this;
    }

    public RestUser build() {
      return new RestUser(this);
    }
  }
}
