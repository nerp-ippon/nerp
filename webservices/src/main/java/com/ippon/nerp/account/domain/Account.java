package com.ippon.nerp.account.domain;

import com.ippon.nerp.authentication.domain.Username;
import com.ippon.nerp.personidentity.domain.Email;
import com.ippon.nerp.personidentity.domain.Firstname;
import com.ippon.nerp.personidentity.domain.Lastname;
import com.ippon.nerp.personidentity.domain.Name;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Account {

  private final Username username;
  private final Email email;
  private final Name name;
  private final Collection<Authority> authorities;

  private Account(AccountBuilder builder) {
    username = new Username(builder.username);
    email = new Email(builder.email);
    name = new Name(builder.firstname, builder.lastname);
    authorities = buildAuthorities(builder.authorities);
  }

  private Collection<Authority> buildAuthorities(Collection<String> authorities) {
    if (authorities == null) {
      return List.of();
    }

    return authorities.stream().map(Authority::new).collect(Collectors.toUnmodifiableSet());
  }

  public static AccountBuilder builder() {
    return new AccountBuilder();
  }

  public Username username() {
    return username;
  }

  public Email email() {
    return email;
  }

  public Name name() {
    return name;
  }

  public Firstname firstname() {
    return name.firstname();
  }

  public Lastname lastname() {
    return name.lastname();
  }

  public Collection<Authority> authorities() {
    return authorities;
  }

  public boolean isInRole(Authority authority) {
    if (authority == null) {
      return false;
    }

    return authorities.contains(authority);
  }

  public static class AccountBuilder {

    private String username;
    private String email;
    private String firstname;
    private String lastname;
    private Collection<String> authorities;

    public AccountBuilder username(String username) {
      this.username = username;

      return this;
    }

    public AccountBuilder email(String email) {
      this.email = email;

      return this;
    }

    public AccountBuilder firstname(String firstname) {
      this.firstname = firstname;

      return this;
    }

    public AccountBuilder lastname(String lastname) {
      this.lastname = lastname;

      return this;
    }

    public AccountBuilder authorities(Collection<String> authorities) {
      this.authorities = authorities;

      return this;
    }

    public Account build() {
      return new Account(this);
    }
  }
}
