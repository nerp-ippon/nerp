package com.ippon.nerp.account.application;

import com.ippon.nerp.account.domain.Account;
import com.ippon.nerp.account.domain.AccountsRepository;
import com.ippon.nerp.common.application.NotSecured;
import org.springframework.stereotype.Service;

@Service
public class AccountsApplicationService {

  private final AccountsRepository accounts;
  private final NotSecuredAccountsApplicationService notSecured;

  public AccountsApplicationService(AccountsRepository accounts) {
    this.accounts = accounts;
    notSecured = new NotSecuredAccountsApplicationService();
  }

  @NotSecured
  public NotSecuredAccountsApplicationService notSecured() {
    return notSecured;
  }

  public class NotSecuredAccountsApplicationService {

    @NotSecured
    public Account authenticatedUser() {
      return accounts.authenticatedUser();
    }
  }
}
