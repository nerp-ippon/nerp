package com.ippon.nerp.account.domain;

import com.ippon.nerp.error.domain.Assert;

public record Authority(String authority) {
  public Authority(String authority) {
    Assert.notBlank("authority", authority);

    this.authority = authority;
  }

  public String get() {
    return authority();
  }
}
