package com.ippon.nerp.account.infrastructure.secondary;

import com.ippon.nerp.account.domain.Account;
import com.ippon.nerp.authentication.infrastructure.primary.NotAuthenticatedUserException;
import com.ippon.nerp.authentication.infrastructure.primary.SecurityUtils;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Component;

@Component
class TokensReader {

  public Account read() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    assertAuthenticated(authentication);

    Map<String, Object> attributes = readAttributes(authentication);

    return Account
      .builder()
      .username(SecurityUtils.getConnectedUserLogin())
      .email((String) attributes.get("email"))
      .firstname((String) attributes.get("given_name"))
      .lastname((String) attributes.get("family_name"))
      .authorities(authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
      .build();
  }

  private void assertAuthenticated(Authentication authentication) {
    if (authentication == null) {
      throw new NotAuthenticatedUserException();
    }
  }

  private Map<String, Object> readAttributes(Authentication authentication) {
    if (authentication instanceof JwtAuthenticationToken) {
      return ((JwtAuthenticationToken) authentication).getTokenAttributes();
    }

    if (authentication instanceof OAuth2AuthenticationToken) {
      return ((OAuth2AuthenticationToken) authentication).getPrincipal().getAttributes();
    }

    throw new NotAuthenticatedUserException();
  }
}
