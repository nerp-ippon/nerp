package com.ippon.nerp.account.domain;

public interface AccountsRepository {
  Account authenticatedUser();
}
