package com.ippon.nerp.authentication.infrastructure.primary;

import com.ippon.nerp.error.domain.ErrorStatus;
import com.ippon.nerp.error.domain.NerpException;

public class NotAuthenticatedUserException extends NerpException {

  public NotAuthenticatedUserException() {
    super(
      NerpException.builder(AuthenticationErrorKey.NOT_AUTHENTICATED).status(ErrorStatus.UNAUTHORIZED).message("User is not authenticated")
    );
  }
}
