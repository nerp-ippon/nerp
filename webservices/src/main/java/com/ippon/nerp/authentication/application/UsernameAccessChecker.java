package com.ippon.nerp.authentication.application;

import com.ippon.nerp.authentication.domain.Username;
import com.ippon.nerp.kipe.AccessChecker;
import com.ippon.nerp.kipe.NerpAuthorizations;
import com.ippon.nerp.kipe.Resource;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
class UsernameAccessChecker implements AccessChecker<Username> {

  @Override
  public boolean can(Authentication authentication, String action, Username item) {
    if (NerpAuthorizations.allAuthorized(authentication, action, Resource.USERS)) {
      return true;
    }

    if (NerpAuthorizations.specificAuthorized(authentication, action, Resource.USERS)) {
      return NerpAuthorizations.getUsername(authentication).equals(item);
    }

    return false;
  }
}
