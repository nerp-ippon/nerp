package com.ippon.nerp.authentication.infrastructure.primary;

import com.ippon.nerp.error.domain.ErrorKey;

enum AuthenticationErrorKey implements ErrorKey {
  NOT_AUTHENTICATED("user.authentication-not-authenticated");

  private final String key;

  AuthenticationErrorKey(String key) {
    this.key = key;
  }

  @Override
  public String key() {
    return key;
  }
}
