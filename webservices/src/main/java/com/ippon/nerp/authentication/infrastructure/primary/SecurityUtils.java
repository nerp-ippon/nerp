package com.ippon.nerp.authentication.infrastructure.primary;

import com.ippon.nerp.authentication.domain.Username;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;

public final class SecurityUtils {

  private static final String ROLE_PREFIX = "ROLE_";

  private SecurityUtils() {}

  public static Username getUsername() {
    return getOptionalUsername().orElseThrow(NotAuthenticatedUserException::new);
  }

  public static Optional<Username> getOptionalUsername() {
    return getOptionalUserLogin().map(Username::new);
  }

  public static String getConnectedUserLogin() {
    return getOptionalUserLogin().orElseThrow(NotAuthenticatedUserException::new);
  }

  public static Optional<String> getOptionalUserLogin() {
    return extractPrincipal(SecurityContextHolder.getContext().getAuthentication());
  }

  public static Optional<String> extractPrincipal(Authentication authentication) {
    if (authentication == null) {
      return Optional.empty();
    }

    if (authentication instanceof JwtAuthenticationToken) {
      Jwt token = ((JwtAuthenticationToken) authentication).getToken();

      return Optional.of((String) token.getClaims().get("preferred_username"));
    }

    Object principal = authentication.getPrincipal();
    if (principal instanceof UserDetails) {
      return Optional.of(((UserDetails) principal).getUsername());
    }

    if (principal instanceof DefaultOidcUser) {
      return Optional.of(((DefaultOidcUser) principal).getPreferredUsername());
    }

    if (principal instanceof OAuth2User) {
      return Optional.of(((OAuth2User) principal).getName());
    }

    if (principal instanceof String) {
      return Optional.of((String) principal);
    }

    return Optional.empty();
  }

  public static List<GrantedAuthority> extractAuthorityFromClaims(Map<String, Object> claims) {
    if (claims == null) {
      return List.of();
    }

    return mapRolesToGrantedAuthorities(getRolesFromClaims(claims));
  }

  @SuppressWarnings("unchecked")
  private static Collection<String> getRolesFromClaims(Map<String, Object> claims) {
    return (Collection<String>) claims.getOrDefault("groups", claims.getOrDefault("roles", List.of()));
  }

  private static List<GrantedAuthority> mapRolesToGrantedAuthorities(Collection<String> roles) {
    return roles.stream().filter(roles()).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
  }

  public static Set<String> getAuthenticatedUserRoles() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    if (authentication == null) {
      return Collections.emptySet();
    }

    return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).filter(roles()).collect(Collectors.toSet());
  }

  private static Predicate<String> roles() {
    return authority -> authority.startsWith(ROLE_PREFIX);
  }
}
