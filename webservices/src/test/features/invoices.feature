Feature: Invoices management

  Scenario: Should not create invoice without being connected
    Given I am not logged in
    When I create default invoice
    Then I should not be authorized
    
  Scenario: Should not create invoice as user
    Given I am logged in as "user"
    When I create default invoice
    Then I should be forbidden
    
  Scenario: Should create invoice as market person
    Given I am logged in as "market-person"
    When I create default invoice
    Then I should have invoice
      | Recipient       | Ippon Technologies |
      | Total           | 183120.0           |
      
  Scenario: Should get invoice
    Given I am logged in as "market-person"
    And I create default invoice
    When I get the created invoice
    Then I should have invoice
      | Recipient       | Ippon Technologies |
      | Total           | 183120.0           |