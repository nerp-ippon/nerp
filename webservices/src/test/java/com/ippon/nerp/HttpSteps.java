package com.ippon.nerp;

import static com.ippon.nerp.cucumber.CucumberAssertions.*;

import io.cucumber.java.en.Then;
import org.springframework.http.HttpStatus;

public class HttpSteps {

  @Then("I should not be authorized")
  public void shouldNotBeAuthorized() {
    assertThatLastResponse().hasHttpStatus(HttpStatus.UNAUTHORIZED);
  }

  @Then("I should be forbidden")
  public void shouldBeForbidden() {
    assertThatLastResponse().hasHttpStatus(HttpStatus.FORBIDDEN);
  }
}
