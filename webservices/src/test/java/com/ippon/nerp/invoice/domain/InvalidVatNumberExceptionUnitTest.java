package com.ippon.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InvalidVatNumberExceptionUnitTest {

  @Test
  void shouldGetExceptionInformation() {
    InvalidVatNumberException exception = new InvalidVatNumberException("invalidNumber");

    assertThat(exception.key()).isEqualTo(InvoiceErrorKey.INVALID_VAT_NUMBER);
    assertThat(exception.arguments()).containsExactly(entry("number", "invalidNumber"));
    assertThat(exception.getMessage()).contains("invalidNumber");
  }
}
