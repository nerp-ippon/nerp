package com.ippon.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import java.util.List;
import java.util.stream.IntStream;
import org.junit.jupiter.api.Test;

class AddressUnitTest {

  @Test
  void shouldNotBuildAddressWithoutLines() {
    assertThatThrownBy(() -> new Address(null)).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("lines");
  }

  @Test
  void shouldNotBuildAddressWithOneLine() {
    assertThatThrownBy(() -> new Address(lines(1))).isExactlyInstanceOf(InvalidAddressException.class);
  }

  @Test
  void shouldNotBuildAddressWith8Lines() {
    assertThatThrownBy(() -> new Address(lines(8))).isExactlyInstanceOf(InvalidAddressException.class);
  }

  @Test
  void shouldGetAddressInformation() {
    Address address = new Address(lines(3));

    assertThat(address.lines()).containsExactly(line(), line(), line());
  }

  @Test
  void shouldGetAddressWith8Lines() {
    Address address = new Address(lines(7));

    assertThat(address.lines()).hasSize(7);
  }

  private static List<AddressLine> lines(int count) {
    return IntStream.range(0, count).mapToObj(value -> line()).toList();
  }

  private static AddressLine line() {
    return new AddressLine("line");
  }
}
