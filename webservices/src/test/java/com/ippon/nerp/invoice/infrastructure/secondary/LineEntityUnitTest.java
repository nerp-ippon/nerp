package com.ippon.nerp.invoice.infrastructure.secondary;

import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import java.util.UUID;
import org.junit.jupiter.api.Test;

class LineEntityUnitTest {

  @Test
  void shouldConvertToNullFromNullInvoice() {
    assertThat(LineEntity.from(null, 0, firstLine())).isNull();
  }

  @Test
  void shouldConvertToNullFromNullLine() {
    assertThat(LineEntity.from(UUID.randomUUID(), 0, null)).isNull();
  }

  @Test
  void shouldConvertFromAndToLine() {
    LineEntity entity = LineEntity.from(invoiceId().get(), 1, firstLine());

    assertThat(entity.toDomain()).usingRecursiveComparison().isEqualTo(firstLine());
  }
}
