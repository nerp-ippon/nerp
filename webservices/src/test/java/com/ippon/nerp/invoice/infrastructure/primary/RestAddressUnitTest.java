package com.ippon.nerp.invoice.infrastructure.primary;

import static com.ippon.nerp.BeanValidationAssertions.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.TestJson;
import com.ippon.nerp.common.infrastructure.primary.ValidationMessage;
import com.ippon.nerp.invoice.domain.InvoicesFixture;
import com.ippon.nerp.invoice.infrastructure.primary.RestAddress.RestAddressBuilder;
import org.junit.jupiter.api.Test;

class RestAddressUnitTest {

  @Test
  void shouldConvertToDomain() {
    assertThat(TestJson.readFromJson(json(), RestAddress.class).toDomain())
      .usingRecursiveComparison()
      .isEqualTo(InvoicesFixture.recipientAddress());
  }

  static String json() {
    return "{\"firstLine\":\"Chez gnuk\",\"city\":\"Lyon\",\"country\":\"FRANCE\"}";
  }

  @Test
  void shouldNotValidateWithoutFirstLine() {
    assertThatBean(fullBuilder().firstLine(null).build())
      .hasInvalidProperty("firstLine")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithBlankFirstLine() {
    assertThatBean(fullBuilder().firstLine(" ").build())
      .hasInvalidProperty("firstLine")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithoutCity() {
    assertThatBean(fullBuilder().city(null).build()).hasInvalidProperty("city").withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithBlankCity() {
    assertThatBean(fullBuilder().city(" ").build()).hasInvalidProperty("city").withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithoutCountry() {
    assertThatBean(fullBuilder().country(null).build())
      .hasInvalidProperty("country")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithBlankCountry() {
    assertThatBean(fullBuilder().country(" ").build()).hasInvalidProperty("country").withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  static RestAddress restAddress() {
    return fullBuilder().build();
  }

  public static RestAddress invalidFirstLine() {
    return fullBuilder().firstLine(null).build();
  }

  private static RestAddressBuilder fullBuilder() {
    return new RestAddressBuilder().firstLine("Chez gnuk").city("Lyon").country("FRANCE");
  }
}
