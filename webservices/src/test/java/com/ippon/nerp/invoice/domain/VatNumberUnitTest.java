package com.ippon.nerp.invoice.domain;

import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import org.junit.jupiter.api.Test;

class VatNumberUnitTest {

  @Test
  void shouldNotBuildWithoutNumber() {
    assertThatThrownBy(() -> new VatNumber(null)).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("number");
  }

  @Test
  void shouldNotBuildWithBlankNumber() {
    assertThatThrownBy(() -> new VatNumber(" ")).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("number");
  }

  @Test
  void shouldNotBuildWithInvalidNumber() {
    assertThatThrownBy(() -> new VatNumber("invalid")).isExactlyInstanceOf(InvalidVatNumberException.class);
  }

  @Test
  void shouldGetVATNumber() {
    VatNumber number = new VatNumber(customerVatNumber());

    assertThat(number.get()).isEqualTo(customerVatNumber());
  }
}
