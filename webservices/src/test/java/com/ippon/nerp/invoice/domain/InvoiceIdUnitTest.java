package com.ippon.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import java.util.UUID;
import org.junit.jupiter.api.Test;

class InvoiceIdUnitTest {

  @Test
  void shouldGenerateIdWithoutId() {
    assertThat(new InvoiceId(null).id()).isNotNull();
  }

  @Test
  void shouldGetId() {
    UUID id = UUID.randomUUID();

    assertThat(new InvoiceId(id).get()).isEqualTo(id);
  }
}
