package com.ippon.nerp.invoice.application;

import static com.ippon.nerp.TestAuthentications.*;
import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class InvoiceIdAccessCheckerUnitTest {

  private static final InvoiceIdAccessChecker checker = new InvoiceIdAccessChecker();

  @Test
  void shouldNotBeAbleToDoUnknownAction() {
    assertThat(checker.can(admin(), "unknown", invoiceId())).isFalse();
  }

  @ParameterizedTest
  @ValueSource(strings = { "ROLE_ADMIN", "ROLE_USER", "ROLE_MARKET" })
  void shouldBeAbleToReadInvoice(String role) {
    assertThat(checker.can(user(role), "read", invoiceId())).isTrue();
  }
}
