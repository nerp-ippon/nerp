package com.ippon.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InvalidInvoiceExceptionUnitTest {

  @Test
  void shouldGetMissingLineException() {
    InvalidInvoiceException exception = InvalidInvoiceException.missingLine();

    assertThat(exception.key()).isEqualTo(InvoiceErrorKey.MISSING_LINES);
    assertThat(exception.getMessage()).contains("line");
  }
}
