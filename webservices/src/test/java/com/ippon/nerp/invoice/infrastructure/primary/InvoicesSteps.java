package com.ippon.nerp.invoice.infrastructure.primary;

import static com.ippon.nerp.cucumber.CucumberAssertions.*;

import com.ippon.nerp.cucumber.CucumberTestContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

public class InvoicesSteps {

  private static final String INVOICE_CREATION_TEMPLATE =
    """
      {
        "recipient": {
          "organisation": "Ippon Technologies",
          "address": {
            "firstLine": "52 Quai Rambaud",
            "city": "Lyon",
            "country": "FRANCE"
          },
          "vatNumber": "AB11111111111",
          "contract": "EA-4251"
        },
        "issuer": "Colin",
        "lines": [
          {
            "designation": "Mon salaire",
            "days": 218,
            "amount": 700
          }
        ]
      }
      """;

  @Autowired
  private TestRestTemplate rest;

  @When("I create default invoice")
  public void createDefaultInvoice() {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    rest.exchange("/api/invoices", HttpMethod.POST, new HttpEntity<>(INVOICE_CREATION_TEMPLATE, headers), Void.class);
  }

  @When("I get the created invoice")
  public void getCreatedInvoice() {
    rest.getForEntity("/api/invoices/" + CucumberTestContext.getElement("$.id"), Void.class);
  }

  @Then("I should have invoice")
  public void shouldHaveInvoice(Map<String, String> invoice) {
    assertThatLastResponse().hasResponse().containing(invoice);
  }
}
