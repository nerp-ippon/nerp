package com.ippon.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InvalidDaysExceptionUnitTest {

  @Test
  void shouldGetExceptionInformation() {
    InvalidDaysException exception = new InvalidDaysException(12.30);

    assertThat(exception.key()).isEqualTo(InvoiceErrorKey.INVALID_DAYS);
    assertThat(exception.arguments()).containsExactly(entry("value", (double) 12.30));
    assertThat(exception.message()).contains("12.3");
  }
}
