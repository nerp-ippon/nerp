package com.ippon.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import com.ippon.nerp.error.domain.StringTooLongException;
import org.junit.jupiter.api.Test;

class AddressLineUnitTest {

  @Test
  void shouldNotBuildWithoutLine() {
    assertThatThrownBy(() -> new AddressLine(null)).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("line");
  }

  @Test
  void shouldNotBuildFromBlankLine() {
    assertThatThrownBy(() -> new AddressLine(" ")).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("line");
  }

  @Test
  void shouldNotBuildFromTooLongLine() {
    assertThatThrownBy(() -> new AddressLine("a".repeat(39)))
      .isExactlyInstanceOf(StringTooLongException.class)
      .hasMessageContaining("line");
  }

  @Test
  void shouldGetLine() {
    assertThat(new AddressLine("line").get()).isEqualTo("line");
  }

  @Test
  void shouldCleanAddressLine() {
    assertThat(new AddressLine(" Chez     Gnuk  C'est \nlà ").get()).isEqualTo("Chez Gnuk C'est là");
  }
}
