package com.ippon.nerp.invoice.domain;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InvalidAddressExceptionUnitTest {

  @Test
  void shouldGetExceptionInformation() {
    InvalidAddressException exception = new InvalidAddressException();

    assertThat(exception.key()).isEqualTo(InvoiceErrorKey.INVALID_ADDRESS);
    assertThat(exception.getMessage()).contains("3").contains("7");
  }
}
