package com.ippon.nerp.invoice.infrastructure.primary;

import static com.ippon.nerp.BeanValidationAssertions.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.TestJson;
import com.ippon.nerp.common.infrastructure.primary.ValidationMessage;
import com.ippon.nerp.invoice.domain.InvoicesFixture;
import com.ippon.nerp.invoice.infrastructure.primary.RestLine.RestLineBuilder;
import org.junit.jupiter.api.Test;

class RestLineUnitTest {

  @Test
  void shouldConvertToDomain() {
    assertThat(TestJson.readFromJson(json(), RestLine.class).toDomain()).usingRecursiveComparison().isEqualTo(InvoicesFixture.firstLine());
  }

  private static String json() {
    return """
        {
          "designation": "This is the first line",
          "days": 1.25,
          "amount": 42.13
        }
        """;
  }

  @Test
  void shouldNotValidateWithoutDesgination() {
    assertThatBean(fullBuilder().designation(null).build())
      .hasInvalidProperty("designation")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithBlankDesgination() {
    assertThatBean(fullBuilder().designation(" ").build())
      .hasInvalidProperty("designation")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  static RestLine restLine() {
    return fullBuilder().build();
  }

  private static RestLineBuilder fullBuilder() {
    return new RestLineBuilder().designation("This is the first line").days(1.25).amount(42.13);
  }
}
