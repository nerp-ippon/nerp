package com.ippon.nerp.invoice.domain;

import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import org.junit.jupiter.api.Test;

class RecipientUnitTest {

  @Test
  void shouldNotBuildWithoutAddress() {
    assertThatThrownBy(() -> recipientBuilder().address(null).build())
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("address");
  }

  @Test
  void shouldGetRecipientInformation() {
    Recipient recipient = recipient();

    assertThat(recipient.name()).isEqualTo(recipientName());
    assertThat(recipient.address()).isEqualTo(recipientAddress());
    assertThat(recipient.customerVatNumber()).isEqualTo(new VatNumber(customerVatNumber()));
    assertThat(recipient.reference()).isEqualTo(new ContractReference("EA-4251"));
  }
}
