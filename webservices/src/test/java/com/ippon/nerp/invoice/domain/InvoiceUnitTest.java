package com.ippon.nerp.invoice.domain;

import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import com.ippon.nerp.invoice.domain.Invoice.InvoiceBuilder;
import org.junit.jupiter.api.Test;

class InvoiceUnitTest {

  @Test
  void shouldNotBuildWithoutId() {
    assertThatThrownBy(() -> invoiceBuilder().id(null).build())
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("id");
  }

  @Test
  void shouldNotBuildWithoutRecipient() {
    assertThatThrownBy(() -> invoiceBuilder().recipient(null).build())
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("recipient");
  }

  @Test
  void shouldNotBuildWithoutIssuer() {
    assertThatThrownBy(() -> invoiceBuilder().issuer(null).build())
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("issuer");
  }

  @Test
  void shouldNotBuildWithoutLine() {
    InvoiceBuilder builder = Invoice.builder().id(InvoiceId.generate()).recipient(recipient()).issuer(issuer());

    assertThatThrownBy(() -> builder.build()).isExactlyInstanceOf(InvalidInvoiceException.class);
  }

  @Test
  void shouldGetInvoiceInformation() {
    Invoice invoice = invoice();

    assertThat(invoice.id()).isEqualTo(invoiceId());
    assertThat(invoice.recipient()).usingRecursiveComparison().isEqualTo(recipient());
    assertThat(invoice.issuer()).isEqualTo(issuer());
    assertThat(invoice.lines()).containsExactly(firstLine(), secondLine());
    assertThat(invoice.total()).isEqualTo(total());
  }

  @Test
  void shouldGetUnmodifiableLines() {
    assertThatThrownBy(() -> invoice().lines().clear()).isExactlyInstanceOf(UnsupportedOperationException.class);
  }
}
