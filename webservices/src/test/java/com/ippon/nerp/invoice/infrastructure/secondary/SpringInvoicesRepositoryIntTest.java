package com.ippon.nerp.invoice.infrastructure.secondary;

import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

@IntegrationTest
class SpringInvoicesRepositoryIntTest {

  @Autowired
  private SpringInvoicesRepository invoices;

  @Test
  void shouldSaveAndGetInvoice() {
    invoices.saveAndFlush(InvoiceEntity.from(invoice()));

    assertThat(invoices.findById(invoiceId().get()).get().toDomain()).usingRecursiveComparison().isEqualTo(invoice());
  }
}
