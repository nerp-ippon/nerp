package com.ippon.nerp.invoice.infrastructure.primary;

import static com.ippon.nerp.BeanValidationAssertions.*;
import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.TestJson;
import com.ippon.nerp.common.infrastructure.primary.ValidationMessage;
import com.ippon.nerp.invoice.infrastructure.primary.RestRecipient.RestRecipientBuilder;
import org.junit.jupiter.api.Test;

class RestRecipientUnitTest {

  @Test
  void shouldConvertToDomain() {
    assertThat(TestJson.readFromJson(json(), RestRecipient.class).toDomain()).usingRecursiveComparison().isEqualTo(recipient());
  }

  private static String json() {
    return (
      "{\"organisation\":\"Gnuk Inc.\",\"address\":" +
      RestAddressUnitTest.json() +
      ",\"vatNumber\":\"FR17451518642\"," +
      "\"contract\":\"EA-4251\"}"
    );
  }

  @Test
  void shouldNotValidateWithoutOrganisation() {
    assertThatBean(fullBuilder().organisation(null).build())
      .hasInvalidProperty("organisation")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithBlankOrganisation() {
    assertThatBean(fullBuilder().organisation(" ").build())
      .hasInvalidProperty("organisation")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithoutAddress() {
    assertThatBean(fullBuilder().address(null).build())
      .hasInvalidProperty("address")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithInvalidAddress() {
    assertThatBean(fullBuilder().address(RestAddressUnitTest.invalidFirstLine()).build()).hasInvalidProperty("address.firstLine");
  }

  @Test
  void shouldNotValidateWithoutVatNumber() {
    assertThatBean(fullBuilder().vatNumber(null).build())
      .hasInvalidProperty("vatNumber")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithBlankVatNumber() {
    assertThatBean(fullBuilder().vatNumber(" ").build())
      .hasInvalidProperty("vatNumber")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithoutContract() {
    assertThatBean(fullBuilder().contract(null).build())
      .hasInvalidProperty("contract")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  @Test
  void shouldNotValidateWithBlankContract() {
    assertThatBean(fullBuilder().contract(" ").build())
      .hasInvalidProperty("contract")
      .withMessage(ValidationMessage.MISSING_MANDATORY_VALUE);
  }

  static RestRecipient invalidContract() {
    return fullBuilder().contract(null).build();
  }

  static RestRecipient restRecipient() {
    return fullBuilder().build();
  }

  private static RestRecipientBuilder fullBuilder() {
    return new RestRecipientBuilder()
      .organisation("Gnuk Inc.")
      .address(RestAddressUnitTest.restAddress())
      .vatNumber("FR17451518642")
      .contract("EA-4251");
  }
}
