package com.ippon.nerp.invoice.infrastructure.secondary;

import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InvoiceEntityUnitTest {

  @Test
  void shouldConvertToNullFromNullDomain() {
    assertThat(InvoiceEntity.from(null)).isNull();
  }

  @Test
  void shouldConvertFromAndToDomain() {
    assertThat(InvoiceEntity.from(invoice()).toDomain()).usingRecursiveComparison().isEqualTo(invoice());
  }
}
