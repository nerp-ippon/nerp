package com.ippon.nerp.invoice.domain;

import com.ippon.nerp.invoice.domain.Invoice.InvoiceBuilder;
import com.ippon.nerp.invoice.domain.Line.LineBuilder;
import com.ippon.nerp.invoice.domain.Recipient.RecipientBuilder;
import java.util.List;
import java.util.UUID;

public final class InvoicesFixture {

  private InvoicesFixture() {}

  public static InvoiceToCreate invoiceToCreate() {
    return InvoiceToCreate.builder().recipient(recipient()).issuer(issuer()).line(firstLine()).line(secondLine()).build();
  }

  public static Invoice invoice() {
    return invoiceBuilder().build();
  }

  static InvoiceBuilder invoiceBuilder() {
    return Invoice.builder().id(invoiceId()).recipient(recipient()).issuer(issuer()).line(firstLine()).line(secondLine());
  }

  public static InvoiceId invoiceId() {
    return new InvoiceId(UUID.fromString("a162b7a4-1bea-48af-ba7b-1cd5ee0ae8ab"));
  }

  public static Total total() {
    return new Total(List.of(firstLine(), secondLine()));
  }

  public static Line firstLine() {
    return lineBuilder().build();
  }

  public static String firstLineDesignation() {
    return "This is the first line";
  }

  public static Line secondLine() {
    return lineBuilder().designation("This is the second line").amount(1337).days(5).build();
  }

  public static LineBuilder lineBuilder() {
    return Line.builder().designation(firstLineDesignation()).days(1.25).amount(42.13).currency(Currency.EUR);
  }

  public static Recipient recipient() {
    RecipientBuilder builder = recipientBuilder();

    return builder.build();
  }

  public static RecipientBuilder recipientBuilder() {
    return Recipient.builder().name("Gnuk Inc.").address(recipientAddress()).customerVatNumber(customerVatNumber()).contract("EA-4251");
  }

  public static OrganisationName recipientName() {
    return new OrganisationName("Gnuk Inc.");
  }

  public static String customerVatNumber() {
    return "FR17451518642";
  }

  public static Address recipientAddress() {
    return new Address(List.of(addressLine("Chez gnuk"), addressLine("Lyon"), addressLine("FRANCE")));
  }

  private static AddressLine addressLine(String line) {
    return new AddressLine(line);
  }

  public static Issuer issuer() {
    return new Issuer("Ippon");
  }
}
