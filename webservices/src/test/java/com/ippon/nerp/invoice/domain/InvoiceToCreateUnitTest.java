package com.ippon.nerp.invoice.domain;

import static com.ippon.nerp.invoice.domain.InvoicesFixture.*;
import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InvoiceToCreateUnitTest {

  @Test
  void shouldCreateInvoice() {
    assertThat(invoiceToCreate().newInvoice()).usingRecursiveComparison().ignoringFields("id").isEqualTo(invoice());
  }
}
