package com.ippon.nerp.account.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.TestJson;
import com.ippon.nerp.account.domain.AccountsFixture;
import com.ippon.nerp.account.infrastructure.primary.RestUser.RestUtilisateurBuilder;
import java.util.List;
import org.junit.jupiter.api.Test;

class RestUserUnitTest {

  @Test
  void shouldConvertToNullFromNullDomain() {
    assertThat(RestUser.from(null)).isNull();
  }

  @Test
  void shouldConvertFromDomain() {
    assertThat(RestUser.from(AccountsFixture.admin())).usingRecursiveComparison().isEqualTo(fullBuilder().build());
  }

  @Test
  void shouldSerializeToJson() {
    assertThat(TestJson.writeAsString(fullBuilder().build()))
      .isEqualTo(
        "{\"username\":\"admin\",\"email\":\"admin@nerp.com\",\"firstname\":\"Jean-Michel\",\"lastname\":\"IAMADMIN\",\"authorities\":[\"ROLE_ADMIN\"]}"
      );
  }

  private static RestUtilisateurBuilder fullBuilder() {
    return RestUser
      .builder()
      .username("admin")
      .email("admin@nerp.com")
      .firstname("Jean-Michel")
      .lastname("IAMADMIN")
      .authorities(List.of("ROLE_ADMIN"));
  }
}
