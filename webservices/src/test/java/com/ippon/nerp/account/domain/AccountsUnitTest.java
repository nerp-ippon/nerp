package com.ippon.nerp.account.domain;

import static com.ippon.nerp.account.domain.AccountsFixture.*;
import static org.assertj.core.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

class AccountsUnitTest {

  @Test
  void shouldGetEmptyAccountsWithoutAccounts() {
    assertThat(new Accounts(null).get()).isEmpty();
  }

  @Test
  void shouldGetSortedAccounts() {
    Account first = adminBuilder().firstname("Aa").build();
    Account second = admin();
    Accounts accounts = new Accounts(List.of(second, first));

    assertThat(accounts.get()).containsExactly(first, second);
  }

  @Test
  void shouldGetUnmodifiableAccounts() {
    assertThatThrownBy(() -> new Accounts(new ArrayList<>()).get().clear()).isExactlyInstanceOf(UnsupportedOperationException.class);
  }
}
