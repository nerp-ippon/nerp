package com.ippon.nerp.account.domain;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import org.junit.jupiter.api.Test;

class AuthorityUnitTest {

  @Test
  void shouldNotBuildWithoutAuthority() {
    assertThatThrownBy(() -> new Authority(null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("authority");
  }

  @Test
  void shouldNotBuildWithBlankAuthority() {
    assertThatThrownBy(() -> new Authority(null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("authority");
  }

  @Test
  void shouldGetAuthority() {
    assertThat(new Authority("ROLE_MANAGER").get()).isEqualTo("ROLE_MANAGER");
  }
}
