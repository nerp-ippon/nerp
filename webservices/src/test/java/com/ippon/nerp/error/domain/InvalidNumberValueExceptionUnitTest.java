package com.ippon.nerp.error.domain;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class InvalidNumberValueExceptionUnitTest {

  @Test
  void shouldGetUnderMinExceptionInformation() {
    InvalidNumberValueException exception = InvalidNumberValueException.underMin("fieldName").value(42).min(1337).build();

    assertThat(exception.key()).isEqualTo(AssertErrorKey.VALUE_UNDER_MIN);
    assertThat(exception.message()).contains("fieldName").contains("42").contains("1337");
    assertThat(exception.arguments()).containsOnly(entry("field", "fieldName"), entry("value", (double) 42), entry("min", (double) 1337));
  }

  @Test
  void shouldGetTooManyDigitsExceptionInformation() {
    InvalidNumberValueException exception = InvalidNumberValueException.tooManyDigits("fieldName", 2);

    assertThat(exception.key()).isEqualTo(AssertErrorKey.TOO_MANY_DIGITS);
    assertThat(exception.message()).contains("fieldName").contains("2");
    assertThat(exception.arguments()).containsOnly(entry("field", "fieldName"), entry("digits", 2));
  }
}
