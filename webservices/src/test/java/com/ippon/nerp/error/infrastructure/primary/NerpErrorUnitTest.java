package com.ippon.nerp.error.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.TestJson;
import java.util.List;
import org.junit.jupiter.api.Test;

class NerpErrorUnitTest {

  @Test
  public void shouldGetErrorInformation() {
    NerpFieldError fieldError = NerpFieldErrorUnitTest.defaultFieldError();
    NerpError error = defaultError(fieldError);

    assertThat(error.getErrorType()).isEqualTo("type");
    assertThat(error.getMessage()).isEqualTo("message");
    assertThat(error.getFieldsErrors()).containsExactly(fieldError);
  }

  @Test
  public void shouldSerializeToJson() {
    assertThat(TestJson.writeAsString(defaultError(NerpFieldErrorUnitTest.defaultFieldError()))).isEqualTo(defaultJson());
  }

  @Test
  void shouldDeserializeFromJson() {
    assertThat(TestJson.readFromJson(defaultJson(), NerpError.class))
      .usingRecursiveComparison()
      .isEqualTo(defaultError(NerpFieldErrorUnitTest.defaultFieldError()));
  }

  private String defaultJson() {
    return "{\"errorType\":\"type\",\"message\":\"message\",\"fieldsErrors\":[" + NerpFieldErrorUnitTest.defaultJson() + "]}";
  }

  private NerpError defaultError(NerpFieldError fieldError) {
    return new NerpError("type", "message", List.of(fieldError));
  }
}
