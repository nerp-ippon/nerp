package com.ippon.nerp.error.infrastructure.primary;

import com.ippon.nerp.error.domain.NerpException;
import com.ippon.nerp.error.domain.StandardErrorKey;

public class PrimaryAdapter {

  public static void fail() {
    throw NerpException.builder(StandardErrorKey.TECHNICAL_ERROR).build();
  }
}
