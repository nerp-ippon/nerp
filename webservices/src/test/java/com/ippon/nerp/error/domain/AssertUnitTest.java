package com.ippon.nerp.error.domain;

import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.List;
import org.junit.jupiter.api.Test;

class AssertUnitTest {

  @Test
  void shouldNotValidateNullObjectAsNotNull() {
    assertThatThrownBy(() -> Assert.notNull("fieldName", null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("fieldName");
  }

  @Test
  void shouldValidateNotNullObjectAsNotNull() {
    assertThatCode(() -> Assert.notNull("field", "toto")).doesNotThrowAnyException();
  }

  @Test
  void shouldNotValidateNullStringAsNotBlank() {
    assertThatThrownBy(() -> Assert.notBlank("fieldName", null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("fieldName");
  }

  @Test
  void shouldNotValidateBlankStringAsNotBlank() {
    assertThatThrownBy(() -> Assert.notBlank("fieldName", " "))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("fieldName");
  }

  @Test
  void shouldValidateActualStringAsNotBlank() {
    assertThatCode(() -> Assert.notBlank("field", "toto")).doesNotThrowAnyException();
  }

  @Test
  void shouldNotValidateNullCollectionAsNotEmpty() {
    assertThatThrownBy(() -> Assert.notEmpty("field", null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("field");
  }

  @Test
  void shouldNotValidateEmptyCollectionAsNotEmpty() {
    assertThatThrownBy(() -> Assert.notEmpty("field", List.of()))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("field");
  }

  @Test
  void shouldValidateActualCollectionAsNotEmpty() {
    assertThatCode(() -> Assert.notEmpty("field", List.of("hey"))).doesNotThrowAnyException();
  }

  @Test
  void shouldNotValidateBlankStringAsNotBlankForFluentAssertions() {
    assertThatThrownBy(() -> Assert.field("fieldName", " ").notBlank())
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("fieldName");
  }

  @Test
  void shouldNotValidateStringOverMaxLength() {
    assertThatThrownBy(() -> Assert.field("fieldName", "value").maxLength(4))
      .isExactlyInstanceOf(StringTooLongException.class)
      .hasMessageContaining("fieldName")
      .hasMessageContaining("5")
      .hasMessageContaining("4");
  }

  @Test
  void shouldValidateMaxLengthForNullValue() {
    assertThatCode(() -> Assert.field("field", null).maxLength(1)).doesNotThrowAnyException();
  }

  @Test
  void shouldValidateShortEnoughString() {
    assertThatCode(() -> Assert.field("field", "value").maxLength(6)).doesNotThrowAnyException();
  }

  @Test
  void shouldValidateStringAtLength() {
    assertThatCode(() -> Assert.field("field", "value").maxLength(5)).doesNotThrowAnyException();
  }

  @Test
  void shouldNotValidateValueUnderMinValue() {
    assertThatThrownBy(() -> Assert.field("field", 42).min(1337))
      .isExactlyInstanceOf(InvalidNumberValueException.class)
      .hasMessageContaining("field")
      .hasMessageContaining("42")
      .hasMessageContaining("1337");
  }

  @Test
  void shouldValidateValueAtMinValue() {
    assertThatCode(() -> Assert.field("field", 1337).min(1337)).doesNotThrowAnyException();
  }

  @Test
  void shouldValidateValueOverMinValue() {
    assertThatCode(() -> Assert.field("field", 10000).min(1337)).doesNotThrowAnyException();
  }

  @Test
  void shouldNotValidateNumberWithTooManyDigits() {
    assertThatThrownBy(() -> Assert.field("field", 1.337).maxDigits(2))
      .isExactlyInstanceOf(InvalidNumberValueException.class)
      .hasMessageContaining("field")
      .hasMessageContaining("2");
  }

  @Test
  void shouldValidateNumberWithDigitsCount() {
    assertThatCode(() -> Assert.field("field", 1.337).maxDigits(3)).doesNotThrowAnyException();
  }

  @Test
  void shouldValidateNumberWithLessThanDigitsCount() {
    assertThatCode(() -> Assert.field("field", 1.33).maxDigits(3)).doesNotThrowAnyException();
  }
}
