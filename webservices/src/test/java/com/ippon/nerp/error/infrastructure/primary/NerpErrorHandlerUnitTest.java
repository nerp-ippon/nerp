package com.ippon.nerp.error.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import ch.qos.logback.classic.Level;
import com.ippon.nerp.LogSpy;
import com.ippon.nerp.error.domain.AssertErrorKey;
import com.ippon.nerp.error.domain.ErrorStatus;
import com.ippon.nerp.error.domain.NerpException;
import com.ippon.nerp.error.domain.StandardErrorKey;
import com.ippon.nerp.message.domain.Messages;
import java.nio.charset.Charset;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

@ExtendWith({ SpringExtension.class, LogSpy.class })
class NerpErrorHandlerUnitTest {

  @Mock
  private Messages messages;

  @InjectMocks
  private NerpErrorHandler handler;

  private final LogSpy logs;

  public NerpErrorHandlerUnitTest(LogSpy logs) {
    this.logs = logs;
  }

  @Test
  public void shouldHandleNerpException() {
    RuntimeException cause = new RuntimeException();
    NerpException exception = NerpException
      .builder(AssertErrorKey.MISSING_MANDATORY_VALUE)
      .cause(cause)
      .status(ErrorStatus.BAD_REQUEST)
      .message("Hum")
      .build();

    when(messages.get(eq("error.missing-mandatory-value"), any())).thenReturn("User message");
    ResponseEntity<NerpError> response = handler.handleNerpException(exception);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

    NerpError body = response.getBody();
    assertThat(body.getErrorType()).isEqualTo("missing-mandatory-value");
    assertThat(body.getMessage()).isEqualTo("User message");
    assertThat(body.getFieldsErrors()).isNull();
  }

  @Test
  public void shouldHandleExceededSizeFileUpload() {
    when(messages.get(eq("error.server.upload-too-big"), any())).thenReturn("The file is too big to be send to the server");

    ResponseEntity<NerpError> response = handler.handleFileSizeException(new MaxUploadSizeExceededException(1024 * 1024 * 30));

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody().getErrorType()).isEqualTo("server.upload-too-big");
    assertThat(response.getBody().getMessage()).isEqualTo("The file is too big to be send to the server");
  }

  @Test
  public void shouldHandleMethodArgumentTypeMismatchException() {
    MethodArgumentTypeMismatchException mismatchException = new MethodArgumentTypeMismatchException(null, null, null, null, null);

    ResponseEntity<NerpError> response = handler.handleMethodArgumentTypeMismatchException(mismatchException);

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    assertThat(response.getBody().getErrorType()).isEqualTo("user.bad-request");
  }

  @Test
  public void shouldHandleMethodArgumentTypeMismatchExceptionWithCause() {
    RuntimeException cause = NerpException.builder(AssertErrorKey.MISSING_MANDATORY_VALUE).build();
    MethodArgumentTypeMismatchException mismatchException = new MethodArgumentTypeMismatchException(null, null, null, null, cause);

    ResponseEntity<NerpError> response = handler.handleMethodArgumentTypeMismatchException(mismatchException);

    assertThat(response.getBody().getErrorType()).isEqualTo("missing-mandatory-value");
  }

  @Test
  public void shouldLogUserErrorAsWarn() {
    handler.handleNerpException(
      NerpException.builder(AssertErrorKey.MISSING_MANDATORY_VALUE).status(ErrorStatus.BAD_REQUEST).message("error message").build()
    );

    logs.assertLogged(Level.WARN, "error message");
  }

  @Test
  public void shouldLogAuthenticationExceptionAsDebug() {
    handler.handleAuthenticationException(new InsufficientAuthenticationException("oops"));

    logs.assertLogged(Level.DEBUG, "oops");
  }

  @Test
  public void shouldLogServerErrorAsError() {
    handler.handleNerpException(
      NerpException.builder(StandardErrorKey.TECHNICAL_ERROR).status(ErrorStatus.INTERNAL_SERVER_ERROR).message("error message").build()
    );

    logs.assertLogged(Level.ERROR, "error message");
  }

  @Test
  public void shouldLogErrorResponseBody() {
    RestClientResponseException cause = new RestClientResponseException(
      "error",
      400,
      "status",
      null,
      "service error response".getBytes(),
      Charset.defaultCharset()
    );

    handler.handleNerpException(
      NerpException
        .builder(StandardErrorKey.TECHNICAL_ERROR)
        .status(ErrorStatus.INTERNAL_SERVER_ERROR)
        .message("error message")
        .cause(cause)
        .build()
    );

    logs.assertLogged(Level.ERROR, "error message");
    logs.assertLogged(Level.ERROR, "service error response");
  }
}
