package com.ippon.nerp.error.infrastructure.primary;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.TestJson;
import org.junit.jupiter.api.Test;

class NerpFieldErrorUnitTest {

  @Test
  public void shouldGetFieldErrorInformation() {
    NerpFieldError fieldError = defaultFieldError();

    assertThat(fieldError.getFieldPath()).isEqualTo("path");
    assertThat(fieldError.getReason()).isEqualTo("reason");
    assertThat(fieldError.getMessage()).isEqualTo("message");
  }

  @Test
  public void shouldSerializeToJson() {
    assertThat(TestJson.writeAsString(defaultFieldError())).isEqualTo(defaultJson());
  }

  static NerpFieldError defaultFieldError() {
    return NerpFieldError.builder().fieldPath("path").reason("reason").message("message").build();
  }

  static String defaultJson() {
    return "{\"fieldPath\":\"path\",\"reason\":\"reason\",\"message\":\"message\"}";
  }
}
