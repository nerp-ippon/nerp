package com.ippon.nerp.error.domain;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StringTooLongExceptionUnitTest {

  @Test
  void shouldGetExceptionInformation() {
    StringTooLongException exception = StringTooLongException.builder().length(50).maxLength(42).field("fieldName").build();

    assertThat(exception.key()).isEqualTo(AssertErrorKey.STRING_TOO_LONG);
    assertThat(exception.arguments()).containsOnly(entry("field", "fieldName"), entry("length", 50), entry("maxLength", 42));
    assertThat(exception.message()).contains("42").contains("50").contains("fieldName");
  }
}
