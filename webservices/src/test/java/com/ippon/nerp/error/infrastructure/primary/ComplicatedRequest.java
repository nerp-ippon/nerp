package com.ippon.nerp.error.infrastructure.primary;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ippon.nerp.common.infrastructure.primary.ValidationMessage;
import javax.validation.constraints.Pattern;

public class ComplicatedRequest {

  private String value;

  public ComplicatedRequest(@JsonProperty("value") String value) {
    this.value = value;
  }

  @Pattern(message = ValidationMessage.WRONG_FORMAT, regexp = "complicated")
  public String getValue() {
    return value;
  }
}
