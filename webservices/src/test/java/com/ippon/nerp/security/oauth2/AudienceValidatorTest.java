package com.ippon.nerp.security.oauth2;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.security.oauth2.jwt.Jwt;

class AudienceValidatorTest {

  private final AudienceValidator validator = new AudienceValidator(Arrays.asList("api://default"));

  @Test
  void shouldNotValidateInvalidAudience() {
    Jwt badJwt = mock(Jwt.class);
    when(badJwt.getAudience()).thenReturn(List.of("bar"));

    assertThat(validator.validate(badJwt).hasErrors()).isTrue();
  }

  @Test
  void shouldValidateValidAudience() {
    Jwt jwt = mock(Jwt.class);
    when(jwt.getAudience()).thenReturn(List.of("api://default"));

    assertThat(validator.validate(jwt).hasErrors()).isFalse();
  }
}
