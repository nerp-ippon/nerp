package com.ippon.nerp.personidentity.domain;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import com.ippon.nerp.error.domain.StringTooLongException;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

class LastnameUnitTest {

  @Test
  void shouldNotBuildWithoutLastname() {
    assertThatThrownBy(() -> new Lastname(null)).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("lastname");
  }

  @Test
  void shouldNotBuildWithBlankLastname() {
    assertThatThrownBy(() -> new Lastname(" ")).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("lastname");
  }

  @Test
  void shouldNotBuildWithTooLongLastname() {
    assertThatThrownBy(() -> new Lastname("a".repeat(101)))
      .isExactlyInstanceOf(StringTooLongException.class)
      .hasMessageContaining("lastname");
  }

  @Test
  void shouldUpperCaseLastname() {
    Lastname lastname = new Lastname("Dupond");

    assertThat(lastname.get()).isEqualTo("DUPOND");
  }

  @Test
  void shouldCompareLastnames() {
    List<Lastname> lastnames = Stream.of(null, new Lastname("DUPONT"), new Lastname("DUPOND")).sorted().toList();

    assertThat(lastnames).containsExactly(new Lastname("DUPOND"), new Lastname("DUPONT"), null);
  }
}
