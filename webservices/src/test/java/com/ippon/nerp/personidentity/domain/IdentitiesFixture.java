package com.ippon.nerp.personidentity.domain;

public final class IdentitiesFixture {

  private IdentitiesFixture() {}

  public static Name name() {
    return new Name(firstname(), lastname());
  }

  public static final Firstname firstname() {
    return new Firstname("Paul");
  }

  public static final Lastname lastname() {
    return new Lastname("DUPOND");
  }
}
