package com.ippon.nerp.personidentity.domain;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import com.ippon.nerp.error.domain.StringTooLongException;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;

class FirstnameUnitTest {

  @Test
  void shouldNotBuildWithoutFirstname() {
    assertThatThrownBy(() -> new Firstname(null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("firstname");
  }

  @Test
  void shouldNotBuildWithBlankFirstname() {
    assertThatThrownBy(() -> new Firstname(" "))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("firstname");
  }

  @Test
  void shouldNotBUildWithTooLongFirstname() {
    assertThatThrownBy(() -> new Firstname("a".repeat(101)))
      .isExactlyInstanceOf(StringTooLongException.class)
      .hasMessageContaining("firstname");
  }

  @Test
  void shouldCapitalizeFirstName() {
    Firstname firstname = new Firstname("jean");

    assertThat(firstname.get()).isEqualTo("Jean");
  }

  @Test
  void shouldCapitalizeComposedFirstName() {
    Firstname firstname = new Firstname("jean-PAUL jÉrémie");

    assertThat(firstname.get()).isEqualTo("Jean-Paul Jérémie");
  }

  @Test
  void shouldSortFirsnames() {
    List<Firstname> firstnames = Stream.of(null, new Firstname("paul"), new Firstname("jean")).sorted().toList();

    assertThat(firstnames).containsExactly(new Firstname("jean"), new Firstname("paul"), null);
  }
}
