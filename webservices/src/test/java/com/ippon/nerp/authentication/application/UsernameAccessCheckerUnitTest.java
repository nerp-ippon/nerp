package com.ippon.nerp.authentication.application;

import static com.ippon.nerp.TestAuthentications.*;
import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.authentication.domain.Username;
import org.junit.jupiter.api.Test;

class UsernameAccessCheckerUnitTest {

  private static final UsernameAccessChecker checker = new UsernameAccessChecker();

  @Test
  void shouldNotBeAuthorizedToReadAnotherUserData() {
    assertThat(checker.can(user(), "read", new Username("another"))).isFalse();
  }

  @Test
  void shouldBeAuthorizedToReadSelfData() {
    assertThat(checker.can(user(), "read", new Username("user"))).isTrue();
  }

  @Test
  void shouldBeAuthorizedToReadAllUserDataForAdmin() {
    assertThat(checker.can(admin(), "read", new Username("any"))).isTrue();
  }
}
