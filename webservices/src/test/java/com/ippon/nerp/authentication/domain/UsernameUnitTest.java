package com.ippon.nerp.authentication.domain;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import com.ippon.nerp.error.domain.StringTooLongException;
import org.junit.jupiter.api.Test;

class UsernameUnitTest {

  @Test
  void shouldNotBuildWithoutUsername() {
    assertThatThrownBy(() -> new Username(null)).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("username");
  }

  @Test
  void shouldNotBuildWithBlankUsername() {
    assertThatThrownBy(() -> new Username(" ")).isExactlyInstanceOf(MissingMandatoryValueException.class).hasMessageContaining("username");
  }

  @Test
  void shouldNotBuildWithTooLongUsername() {
    assertThatThrownBy(() -> new Username("a".repeat(51)))
      .isExactlyInstanceOf(StringTooLongException.class)
      .hasMessageContaining("username");
  }

  @Test
  void shouldGetEmptyUsernameFromNullUsername() {
    assertThat(Username.of(null)).isEmpty();
  }

  @Test
  void shouldGetEmptyUsernameFromBlankUsername() {
    assertThat(Username.of(" ")).isEmpty();
  }

  @Test
  void shouldGetUsernameFromActualUsername() {
    assertThat(Username.of("user")).contains(new Username("user"));
  }

  @Test
  void shouldGetUsername() {
    Username username = new Username("user");

    assertThat(username.get()).isEqualTo("user");
  }
}
