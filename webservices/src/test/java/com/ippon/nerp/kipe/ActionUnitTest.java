package com.ippon.nerp.kipe;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.error.domain.MissingMandatoryValueException;
import org.junit.jupiter.api.Test;

class ActionUnitTest {

  @Test
  void shouldNotBuildAllActionWithoutAction() {
    assertThatThrownBy(() -> Action.all(null, Resource.USERS))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("action");
  }

  @Test
  void shouldNotBuildAllActionWithBlankAction() {
    assertThatThrownBy(() -> Action.all(" ", Resource.USERS))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("action");
  }

  @Test
  void shouldNotBuildAllActionWithoutResource() {
    assertThatThrownBy(() -> Action.all("action", null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("resource");
  }

  @Test
  void shouldNotBuildSpecificActionWithoutAction() {
    assertThatThrownBy(() -> Action.specific(null, Resource.USERS))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("action");
  }

  @Test
  void shouldNotBuildSpecificActionWithBlankAction() {
    assertThatThrownBy(() -> Action.specific(" ", Resource.USERS))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("action");
  }

  @Test
  void shouldNotBuildSpecificActionWithoutResource() {
    assertThatThrownBy(() -> Action.specific("action", null))
      .isExactlyInstanceOf(MissingMandatoryValueException.class)
      .hasMessageContaining("resource");
  }

  @Test
  void shouldNotBeEqualToNull() {
    assertThat(action().equals(null)).isFalse();
  }

  @Test
  @SuppressWarnings("unlikely-arg-type")
  void shouldNotBeEqualToAnotherType() {
    assertThat(action().equals("action")).isFalse();
  }

  @Test
  void shouldBeEqualToSelf() {
    Action action = action();

    assertThat(action.equals(action)).isTrue();
  }

  @Test
  public void shouldNotBeEqualToActionWithAnotherAction() {
    assertThat(action().equals(Action.all("dummy", Resource.USERS))).isFalse();
  }

  @Test
  public void shouldNotBeEqualToActionWithAnotherResource() {
    assertThat(action().equals(Action.all("action", Resource.INVOICES))).isFalse();
  }

  @Test
  public void shouldNotBeEqualToActionWithAnotherAuthorization() {
    assertThat(action().equals(Action.specific("action", Resource.USERS))).isFalse();
  }

  @Test
  public void shouldBeEqualToActionWithSameActionResourceAndAuthorization() {
    assertThat(action().equals(action())).isTrue();
  }

  @Test
  public void shouldGetSameHashCodeWithSameAction() {
    assertThat(action().hashCode()).isEqualTo(action().hashCode());
  }

  @Test
  public void shouldGetDifferentHashCodeWithDifferentAction() {
    assertThat(action().hashCode()).isNotEqualTo(Action.all("dummy", Resource.USERS).hashCode());
  }

  @Test
  public void shouldGetDifferentHashCodeWithDifferentResource() {
    assertThat(action().hashCode()).isNotEqualTo(Action.all("action", Resource.INVOICES).hashCode());
  }

  @Test
  public void shouldGetDifferentHashCodeWithDifferentAuthorization() {
    assertThat(action().hashCode()).isNotEqualTo(Action.specific("action", Resource.USERS).hashCode());
  }

  private static Action action() {
    return Action.all("action", Resource.USERS);
  }
}
