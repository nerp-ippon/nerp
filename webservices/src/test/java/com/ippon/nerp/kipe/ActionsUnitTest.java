package com.ippon.nerp.kipe;

import static org.assertj.core.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ActionsUnitTest {

  @Test
  void shouldNotBuildWithDuplicatedActions() {
    assertThatThrownBy(() -> new Actions(Action.all("action", Resource.USERS), Action.all("action", Resource.USERS)))
      .isExactlyInstanceOf(IllegalArgumentException.class);
  }

  @Test
  void shouldNotBeAuthorizedWithoutAction() {
    assertThat(new Actions((Action[]) null).allAuthorized("action", Resource.USERS)).isFalse();
  }

  @Test
  void shouldCheckAuthorizations() {
    Actions actions = new Actions(Action.all("action", Resource.USERS), Action.specific("action", Resource.INVOICES));

    assertThat(actions.allAuthorized("dummy", Resource.USERS)).isFalse();
    assertThat(actions.allAuthorized("action", Resource.USERS)).isTrue();
    assertThat(actions.allAuthorized("action", Resource.INVOICES)).isFalse();
    assertThat(actions.specificAuthorized("dummy", Resource.USERS)).isFalse();
    assertThat(actions.specificAuthorized("action", Resource.USERS)).isTrue();
    assertThat(actions.specificAuthorized("action", Resource.INVOICES)).isTrue();
  }
}
