package com.ippon.nerp.kipe;

import static org.assertj.core.api.Assertions.*;

import com.ippon.nerp.authentication.domain.Authorities;
import com.ippon.nerp.authentication.domain.Username;
import com.ippon.nerp.authentication.infrastructure.primary.NotAuthenticatedUserException;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;

class NerpAuthorizationsUnitTest {

  @Test
  void shouldNotGetUsernameWithoutAuthentication() {
    assertThatThrownBy(() -> NerpAuthorizations.getUsername(null)).isExactlyInstanceOf(NotAuthenticatedUserException.class);
  }

  @Test
  void shouldGetUsernameFromAuthentication() {
    assertThat(NerpAuthorizations.getUsername(user())).isEqualTo(new Username("user"));
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingWithoutAuthentication() {
    assertThat(NerpAuthorizations.allAuthorized(null, "action", Resource.USERS)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingWithoutAction() {
    assertThat(NerpAuthorizations.allAuthorized(user(), null, Resource.USERS)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingWithBlankAction() {
    assertThat(NerpAuthorizations.allAuthorized(user(), " ", Resource.USERS)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingWithoutResource() {
    assertThat(NerpAuthorizations.allAuthorized(user(), "action", null)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingForUnauthorizedAction() {
    assertThat(NerpAuthorizations.allAuthorized(user(), "save", Resource.USERS)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingForSpecificAuthorizedAction() {
    assertThat(NerpAuthorizations.allAuthorized(user(), "read", Resource.USERS)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingForAllUnauthorizedNoRoleAction() {
    assertThat(NerpAuthorizations.allAuthorized(admin(), "write", Resource.USERS)).isFalse();
  }

  @Test
  void shouldNotBeAllAuthorizedWhenCheckingForUnknownAction() {
    assertThat(NerpAuthorizations.allAuthorized(admin(), "dummy", Resource.USERS)).isFalse();
  }

  @Test
  void shouldBeAllAuthorizedWhenCheckingForAllAuthorizedNoRoleAction() {
    assertThat(NerpAuthorizations.allAuthorized(admin(), "read", Resource.INVOICES)).isTrue();
  }

  @Test
  void shouldBeAllAuthorizedWhenCheckingForAllAuthorizedAction() {
    assertThat(NerpAuthorizations.allAuthorized(admin(), "save", Resource.USERS)).isTrue();
  }

  @Test
  void shouldNotBeSpecificAuthorizedWhenCheckingWithoutAuthentication() {
    assertThat(NerpAuthorizations.specificAuthorized(null, "action", Resource.USERS)).isFalse();
  }

  @Test
  void shouldNotBeSpecificAuthorizedWhenCheckingWithoutAction() {
    assertThat(NerpAuthorizations.specificAuthorized(user(), null, Resource.USERS)).isFalse();
  }

  @Test
  void shouldNotBeSpecificAuthorizedWhenCheckingWithBlankAction() {
    assertThat(NerpAuthorizations.specificAuthorized(user(), " ", Resource.USERS)).isFalse();
  }

  @Test
  void shouldNotBeSpecificAuthorizedWhenCheckingWithoutResource() {
    assertThat(NerpAuthorizations.specificAuthorized(user(), "action", null)).isFalse();
  }

  @Test
  void shouldNotBeSpecificAuthorizedWhenCheckingForUnauthorizedAction() {
    assertThat(NerpAuthorizations.specificAuthorized(user(), "update", Resource.USERS)).isFalse();
  }

  @Test
  void shouldNotBeSpecificAuthorizedWhenCheckingForUnknownAction() {
    assertThat(NerpAuthorizations.specificAuthorized(admin(), "dummy", Resource.USERS)).isFalse();
  }

  @Test
  void shouldBeSpecificAuthorizedWhenCheckingForSpecificAuthorizedNoRoleAction() {
    assertThat(NerpAuthorizations.specificAuthorized(user(), "read", Resource.USERS)).isTrue();
  }

  @Test
  void shouldBeSpecificAuthorizedWhenCheckingForAllAuthorizedAction() {
    assertThat(NerpAuthorizations.specificAuthorized(admin(), "save", Resource.USERS)).isTrue();
  }

  private Authentication user() {
    return new TestingAuthenticationToken("user", null, Authorities.USER);
  }

  private Authentication admin() {
    return new TestingAuthenticationToken("gerard", null, Authorities.ADMIN);
  }
}
