# NERP

NERP is not an ERP.

Just a toy application trying to put business value in the center of something traditionally data centric: an ERP

## Replays

- [On commence les factures - Pauline et Colin](https://www.twitch.tv/videos/979931500)
- [On continue les factures - Raphaël et Colin](https://www.twitch.tv/videos/979932104)
- [Encore les factures - Salomé et Colin](https://www.twitch.tv/videos/1001511714)
- [Gestion des dates - Colin](https://www.twitch.tv/videos/1020911369)
- [NERP dans JHipster](https://www.twitch.tv/videos/1035604580)
- [On refais du NERP](https://www.twitch.tv/videos/1192999371)
- [On continue NERP (pas beaucoup de code fait dans ce live)](https://www.twitch.tv/videos/1193128040)

## Projects

- [WebServices](webservices): WebServices for NERP

## Developpment

You'll need:

- [JDK 17](https://openjdk.java.net/install/);
- [npm](https://www.npmjs.com/) (use [nvm](https://github.com/nvm-sh/nvm), it's easier).

### Formatting

This projet uses prettier for code formatting so you'll have to install git hooks by doing:

```sh
npm i
```

### Makefile

There is a local makefile for common operation:

- `make`: start the whole stack
- `make stop`: stop the stack
- `make clean` clean leftovers
- `make format`: force a full reformatting
