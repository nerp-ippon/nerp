.PHONY: dev

dev: start-webservices
stop: stop-webservices

start-webservices: stop-webservices
	docker-compose -f webservices/src/main/docker/keycloak.yml up -d > webservices/stack.log
	cd webservices && { ./mvnw & echo $$! > webservices.pid;} > webservices.log &
	echo "Webservices starting (can take a few minutes, cf tail -f webservices/webservices.log)"

stop-webservices: 
ifneq ("$(wildcard webservices/webservices.pid)","")
	docker-compose -f webservices/src/main/docker/keycloak.yml down --remove-orphans
	kill `cat webservices/webservices.pid` && rm webservices/webservices.pid
endif

watch-logs:
	tail -f webservices/stack.log webservices/webservices.log
	
clean: clean-webservices

clean-webservices: 
	cd webservices && ./mvnw clean
	rm -f webservices/webservices.pid

format:
	npm run prettier:format
